<?php
// $resource_types = get_the_terms( $post->ID , 'resource-type' );
// $resource_type = $resource_types[0]->slug;
// $resource_term_id = $resource_types[0]->term_id;

// $related_resources_title = '';
// switch($resource_type) {
// case "area":
// $related_resources_title = $resource_type . 's';
// break;

// case "individual":
// $related_resources_title = $resource_type . 's';
// break;

// case "managers":
// $related_resources_title = $resource_type . 's';
// break;
// }
?>

<section>
    <div class="container-lg">

        <div class="row">
            <div class="col-12 col-md-8 content">

                <h1 class="mb-1"><?php echo e(the_title()); ?></h1>
                <div class="meta mb-3"><?php echo e(get_reading_time(get_the_content())); ?> min read</div>

                <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'w-100 h-auto mb-3 mb-md-5', 'alt' => get_the_title()]); ?>


                <?php(the_content()) ?>

                <div class="exclude-section-padding">
                    <?php echo $__env->make('parts.components', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
            <div class="col-12 col-md-4">

                <?php echo $__env->make('parts.sidebar-components', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <?php
                    
                    $tags = [];
                    $tag_data = get_the_tags(get_queried_object()->ID);
                    foreach ($tag_data as $key => $tag) {
                        array_push($tags, $tag->term_id);
                    }
                    
                    $args = [
                        'post_type' => 'resource',
                        'tag__in' => $tags,
                        'post__not_in' => [get_queried_object()->ID],
                        'posts_per_page' => 5,
                        'orderby' => 'publish_date',
                        'order' => 'DESC',
                    ];
                    
                    $query = new WP_Query($args);
                    
                ?>

                <?php if($query->have_posts()): ?>

                    <h2>Related</h2>

                    <div class="resource-container">

                        <div class="mt-4">

                            <?php while($query->have_posts()): ?>
                                <?php $query->the_post() ?>

                                <?php
                                    $category_meta = get_categories_meta(get_post(), 'resource-topic');
                                ?>

                                <div class="row mb-5">
                                    <div class="col-12 col-md-6">

                                        <a href="<?php echo e(the_permalink()); ?>">
                                            <?php if(get_post_thumbnail_id()): ?>
                                                <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'square', false, ['class' => 'w-100 h-auto rounded', 'alt' => get_sub_field('title')]); ?>

                                            <?php else: ?>
                                                <img src="<?= App\asset_path('images/nwh-default.png'); ?>" class="w-100 h-auto rounded"
                                                    alt="<?php echo e(get_sub_field('title')); ?>">
                                            <?php endif; ?>
                                        </a>

                                    </div>
                                    <div class="col-12 col-md-6">
                                        <?php if($category_meta['parent']): ?>
                                            <span class="meta"><?php echo $category_meta['parent']; ?></span>
                                        <?php endif; ?>
                                        <h5 class="mb-1"><a href="<?php echo e(the_permalink()); ?>"><?php echo e(the_title()); ?></a></h5>
                                        <div class="meta mb-3"><?php echo e(get_reading_time(get_the_content())); ?> min read
                                        </div>

                                    </div>
                                </div>

                            <?php endwhile; ?>

                        </div>

                    </div>

                <?php endif; ?>

                <?php wp_reset_postdata() ?>

                <h2>Most popular</h2>

                <?php
                    
                    $category = get_queried_object();
                    
                    $popular_resources = pvc_get_most_viewed_posts([
                        'order' => 'desc',
                        'post_type' => ['resource'],
                        'tax_query' => [
                            [
                                'taxonomy' => 'resource-topic',
                                'field' => 'id',
                                'terms' => $category->term_id,
                            ],
                        ],
                    ]);
                    
                ?>

                <?php if($popular_resources): ?>

                    <h2>Most popular</h2>

                    <?php $__currentLoopData = $popular_resources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <?php
                            $category_meta = get_categories_meta($post, 'resource-topic');
                        ?>

                        <div class="row no-gutters mb-5">
                            <div class="col-4">

                                <a href="<?php echo e(the_permalink()); ?>">
                                    <?php if(get_post_thumbnail_id($post->ID)): ?>
                                        <?php echo wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'square', false, ['class' => 'w-100 h-auto rounded', 'alt' => get_the_title($post->ID)]); ?>

                                    <?php else: ?>
                                        <img src="<?= App\asset_path('images/nwh-default.png'); ?>" class="w-100 h-auto rounded"
                                            alt="<?php echo e(get_sub_field('title')); ?>">
                                    <?php endif; ?>
                                </a>

                            </div>
                            <div class="col-8">
                                <div class="ml-4">
                                    <?php if($category_meta['parent']): ?>
                                        <span class="meta"><?php echo $category_meta['parent']; ?></span>
                                    <?php endif; ?>
                                    <h5 class="mb-1"><a
                                            href="<?php echo e(get_the_permalink($post->ID)); ?>"><?php echo e(get_the_title($post->ID)); ?></a>
                                    </h5>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

            </div>
        </div>

    </div>
</section>

<?php if($resource_type): ?>

    <section class="bg-white">
        <div class="container-lg">
            <h2 class="mb-4 mb-md-5">Other resources for <?php echo e($related_resources_title); ?></h1>
                <?php echo $__env->make('partials.related-resources', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </section>

<?php endif; ?>
