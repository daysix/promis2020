<?php 

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $args = array(
        'posts_per_page' => get_option('posts_per_page'),
        'paged' => $paged,
        'post_type'	=> 'resource'
        
    );

    if($_POST['term']){
        $args['s'] = $_POST['term'];
    }

    if(!empty($_POST['type']) || !empty($_POST['topic'])){

        $filter['tax_query'] = array();
       
        if(!empty($_POST['type'])){
            array_push(
                $filter['tax_query'],
                array(
                    'taxonomy' => 'resource-type',
                    'field'    => 'term_id',
                    'terms'    => $_POST['type'],
                )
            );
        }
        
        if(!empty($_POST['topic'])){
            array_push(
                $filter['tax_query'],
                array(
                    'taxonomy' => 'resource-topic',
                    'field'    => 'term_id',
                    'terms'    => $_POST['topic'],
                )
            );
        }

        if(count($filter['tax_query']) > 1){
            $filter['tax_query']['relation'] = 'OR';
        }

        $args = array_merge($filter, $args);
        
    }
   
    $query = new WP_Query($args);

?>

<? if(!empty( get_the_content())){ 
  $content_width = (get_field('content_width') ? $content_width = get_field('content_width') : 'container');
?>

  <section class="bg-white">
    <div class="content <?php echo $content_width; ?>">
      <?php(the_content())
    </div>
  </section>
  
  <?php } ?>

  <section class="bg-gray-light">
    <div class="container-lg resource-filters">

        <form action="" method="post" id="resource-filter" class="form-group-lg">
            
            <div class="row mb-4">
                
                <div class="col-12 col-sm-4">
                   
                    <span class="meta text-primary">Resource type</span><br />

                      <?php 

                        $terms = get_terms([
                            'taxonomy' => 'resource-type',
                            'hide_empty' => false
                        ]); 

                      ?>

                      <select name="type[]" class="selectpicker show-tick mb-4"  multiple data-max-options="1" data-style="btn-white" data-width="100%">  
                        <?php foreach($terms as $term) { ?>
                          <option value="<?php echo $term->term_id; ?>"  <?php if($_POST['type'] && in_array($term->term_id, $_POST['type'])){ ?>selected<?php } ?>><?php echo $term->name; ?></option>
                        <?php } ?>
                        </select>

                </div>

                <div class="col-12 col-sm-4 pr-4">
                
                  <span class="meta text-primary">Resource topic</span><br />
                  
                  <?php 

                    $terms = get_terms([
                      'taxonomy' => 'resource-topic',
                      'hide_empty' => false
                    ]); 

                  ?>

                  <select name="topic[]" class="selectpicker show-tick mb-4"  multiple data-max-options="1" data-style="btn-white" data-width="100%"> 
                    <?php foreach($terms as $term) { ?>
                      <option value="<?php echo $term->term_id; ?>" <?php if($_POST['topic'] && in_array($term->term_id, $_POST['topic'])){ ?>selected<?php } ?>><?php echo $term->name; ?></option>
                    <?php } ?>
                  </select>

                </div>

                <div class="col-12 col-sm-4">
                   
                <span class="meta text-primary">Looking for something specific?</span><br />
                    <div style="position:relative">

                        <?php 
                          $new_term = stripslashes_deep($_POST['term']);
                        ?>
                        <input type="text" name="term" id="term" value="<?php echo $new_term; ?>" placeholder="Search term..." class="form-control bg-white border-0">

                        
                        <span id="term_cancel" <?php if(!$_POST['term']){ ?>style="visibility: hidden"<?php } ?>><i class="fas fa-times-circle text-primary"></i></span>
                    </div>

                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Filter">
            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-link"><span class="meta text-primary">Reset</span></a>
        </form>

    </div>
</section>

<section>
  <div class="container-lg">

<?php if( $query->have_posts() ): ?>

    <div class="resource-container">

        <div class="row mt-4">

            <?php while( $query->have_posts() ) : $query->the_post(); ?>

            @php
                $category_meta = get_categories_meta(get_post(), 'resource-topic');
            ?>

            <div class="col-md-4 col-sm-6 col-12">

                    <a href="<?php echo e(the_permalink()); ?>">
                      <?php if(get_post_thumbnail_id()): ?>
                          <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'w-100 h-auto rounded', 'alt' => get_sub_field('title')]); ?>

                      <?php else: ?>
                          <img src="<?= App\asset_path('images/nwh-default.png'); ?>" class="w-100 h-auto rounded"
                              alt="<?php echo e(get_sub_field('title')); ?>">
                      <?php endif; ?>
                  </a>

                    <?php if($category_meta['parent']): ?>
                      <span class="meta"><?php echo $category_meta['parent']; ?></span>
                    <?php endif; ?>
                    <h4 class="mb-1"><a href="<?php echo e(the_permalink()); ?>"><?php echo e(the_title()); ?></a></h4>
                    <div class="meta mb-3"><?php echo e(get_reading_time(get_the_content())); ?> min read</div>
                    <p><?php echo e(the_excerpt()); ?></p>

                  <a href="<?php the_permalink(); ?>">Read more</a>

                </div> 
                
            <?php endwhile; ?>
        
        </div>

    </div>
  

<?php endif; ?>

<?php wp_reset_query();	 // Restore global post data  ?>

  </div>
</section>

<?php echo $__env->make('parts.components', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>