<?php $__env->startSection('content'); ?>
    <?php while(have_posts()): ?> <?php the_post() ?>

        <section>
            <div class="container-lg">
                <h2>Featured resources</h2>
                <div class="row">
                    <div class="col-12 col-md-8">

                        <?php if(have_rows('featured_resources')): ?>
                            <?php while(have_rows('featured_resources')): ?> <?php the_row() ?>

                                <?php
                                    $resource = get_sub_field('resource');
                                    $category_meta = get_categories_meta(get_post($resource->ID), 'resource-topic');
                                ?>

                                <div class="row mb-5">
                                    <div class="col-12 col-md-6">

                                        <a href="<?php echo e(get_the_permalink($resource->ID)); ?>">
                                            <?php if(get_post_thumbnail_id($resource->ID)): ?>

                                                <?php echo wp_get_attachment_image(get_post_thumbnail_id($resource->ID), 'full', false, ['class' => 'w-100 h-auto rounded', 'alt' => get_the_title($resource->ID)]); ?>

                                            <?php else: ?>
                                                <img src="<?= App\asset_path('images/nwh-default.png'); ?>" class="w-100 h-auto rounded"
                                                    alt="<?php echo e(get_the_title($resource->ID)); ?>">
                                            <?php endif; ?>
                                        </a>

                                    </div>
                                    <div class="col-12 col-md-6">
                                        <?php if($category_meta['parent']): ?>
                                            <span class="meta"><?php echo $category_meta['parent']; ?></span>
                                        <?php endif; ?>
                                        <h4 class="mb-1"><a
                                                href="<?php echo e(get_the_permalink($resource->ID)); ?>"><?php echo e(get_the_title($resource->ID)); ?></a>
                                        </h4>
                                        <div class="meta mb-3"><?php echo e(get_reading_time($resource->post_content)); ?> min read
                                        </div>
                                        <p>
                                            <?php echo get_the_excerpt($resource); ?>

                                        </p>
                                        <a href="<?php echo e(get_the_permalink($resource->ID)); ?>">Read more</a>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                    <div class="col-12 col-md-4">
                        <h3>What are you looking for?</h3>
                        <p>We've curated some of the most popular resources in the areas below</p>

                        <?php
                            $terms = get_terms('audiences', ['hide_empty' => false]);
                        ?>
                        <?php $__currentLoopData = $terms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="icon">
                                <img src="<?php echo e(the_field('audience-icon', 'term_' . $term->term_id)); ?>">
                                <div>
                                    <h4><a href="<?php echo e(get_term_link($term->term_id)); ?>"><?php echo e($term->description); ?></a></h4>
                                </div>
                            </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <?php if(have_rows('related_areas')): ?>

                            <h3>Areas of interest</h3>
                            <div class="tags mt-3 mb-4">

                                <?php while(have_rows('related_areas')): ?>
                                    <?php(the_row()) ?>

                                    <?php
                                        if (get_sub_field('type') == 'link') {
                                            $title = get_sub_field('title');
                                            $link = get_sub_field('link');
                                        } else {
                                            $topic = get_sub_field('topic');
                                            $title = $topic->name;
                                            $link = get_term_link($topic);
                                        }
                                    ?>

                                    <a href="<?php echo e($link); ?>" class="btn btn-secondary"><?php echo e($title); ?></a>

                                <?php endwhile; ?>

                            </div>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </section>

        <section class="bg-gradient-blue">
            <div class="container-lg">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h3>Thank you</h3>
                        <p>"I want to thank you, thank everyone working across Scotland to provide care for others. Wherever
                            you work, whatever your role – the work you are doing is vital. You’re doing a great job of
                            looking after us, so please try to let us do a little bit to support you too."<br />
                            Clare Haughey, Minister for Mental Health</p>
                    </div>
                    <div class="col-12 col-md-6"></div>
                </div>
            </div>
        </section>

        <section>
            <div class="container-lg">

                <div class="row">
                    <div class="col-12 col-md-8">
                        <h2>Latest on the hub</h2>

                        <?php
                            
                            $args = [
                                // 'posts_per_page' => get_option('posts_per_page'),
                                'post_type' => 'resource',
                                'posts_per_page' => 5,
                                'orderby' => 'publish_date',
                                'order' => 'desc',
                            ];
                            
                            $query = new WP_Query($args);
                            
                        ?>

                        <?php if($query->have_posts()): ?>

                            <div class="resource-container">

                                <div class="mt-4">

                                    <?php while($query->have_posts()): ?>
                                        <?php $query->the_post() ?>

                                        <?php
                                            $category_meta = get_categories_meta(get_post(), 'resource-topic');
                                        ?>

                                        <div class="row mb-5">
                                            <div class="col-12 col-md-6">

                                                <a href="<?php echo e(the_permalink()); ?>">
                                                    <?php if(get_post_thumbnail_id()): ?>
                                                        <?php echo wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'w-100 h-auto rounded', 'alt' => get_sub_field('title')]); ?>

                                                    <?php else: ?>
                                                        <img src="<?= App\asset_path('images/nwh-default.png'); ?>"
                                                            class="w-100 h-auto rounded"
                                                            alt="<?php echo e(get_sub_field('title')); ?>">
                                                    <?php endif; ?>
                                                </a>

                                            </div>
                                            <div class="col-12 col-md-6">
                                                <?php if($category_meta['parent']): ?>
                                                    <span class="meta"><?php echo $category_meta['parent']; ?></span>
                                                <?php endif; ?>
                                                <h4 class="mb-1"><a href="<?php echo e(the_permalink()); ?>"><?php echo e(the_title()); ?></a>
                                                </h4>
                                                <div class="meta mb-3"><?php echo e(get_reading_time(get_the_content())); ?> min read
                                                </div>

                                                <?php echo e(the_excerpt()); ?>

                                                <a href="<?php echo e(the_permalink()); ?>">Read more</a>

                                            </div>
                                        </div>

                                    <?php endwhile; ?>

                                </div>

                            </div>

                        <?php endif; ?>

                        <?php wp_reset_postdata() ?>

                        

                    </div>
                    <div class="col-12 col-md-4">
                        <h3>Most popular</h3>

                        <?php
                            
                            $popular_resources = pvc_get_most_viewed_posts([
                                'order' => 'desc',
                                'post_type' => ['resource'],
                            ]);
                            
                        ?>

                        <?php if($popular_resources): ?>
                            <?php $__currentLoopData = $popular_resources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    $category_meta = get_categories_meta($post, 'resource-topic');
                                ?>

                                <div class="row no-gutters mb-5">
                                    <div class="col-4">


                                        <a href="<?php echo e(the_permalink()); ?>">
                                            <?php if(get_post_thumbnail_id($post->ID)): ?>
                                                <?php echo wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'square', false, ['class' => 'w-100 h-auto rounded', 'alt' => get_the_title($post->ID)]); ?>

                                            <?php else: ?>
                                                <img src="<?= App\asset_path('images/nwh-default.png'); ?>" class="w-100 h-auto rounded"
                                                    alt="<?php echo e(get_sub_field('title')); ?>">
                                            <?php endif; ?>
                                        </a>

                                    </div>
                                    <div class="col-8">
                                        <div class="ml-4">
                                            <?php if($category_meta['parent']): ?>
                                                <span class="meta"><?php echo $category_meta['parent']; ?></span>
                                            <?php endif; ?>
                                            <h5 class="mb-1"><a
                                                    href="<?php echo e(get_the_permalink($post->ID)); ?>"><?php echo e(get_the_title($post->ID)); ?></a>
                                            </h5>
                                        </div>
                                    </div>
                                </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </section>

        

    <?php endwhile; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>