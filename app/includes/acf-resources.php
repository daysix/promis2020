<?php

if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(array(
    'key' => 'group_5e9f1c20efa7e',
    'title' => 'Resource',
    'fields' => array(
      array(
        'key' => 'field_5e9f1de6e3736',
        'label' => 'Image',
        'name' => 'resource-image',
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'acfe_permissions' => '',
        'acfe_uploader' => 'wp',
        'acfe_thumbnail' => 0,
        'return_format' => 'id',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
      ),
      array(
        'key' => 'field_5eb5302b51565',
        'label' => 'Icon',
        'name' => 'resource-icon',
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'acfe_permissions' => '',
        'acfe_uploader' => 'wp',
        'acfe_thumbnail' => 0,
        'return_format' => 'url',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
      ),
      array(
        'key' => 'field_5eb71025d0a25',
        'label' => 'Header background',
        'name' => 'resource-header-background',
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'acfe_permissions' => '',
        'acfe_uploader' => 'wp',
        'acfe_thumbnail' => 0,
        'return_format' => 'url',
        'preview_size' => 'medium',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'resource',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'seamless',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
    'acfe_display_title' => '',
    'acfe_autosync' => array(
      0 => 'php',
    ),
    'acfe_permissions' => '',
    'acfe_form' => 0,
    'acfe_meta' => '',
    'acfe_note' => '',
  ));


 // add_action('init', 'add_fc_layout', 20);
  // function add_fc_layout() {
    //echo 'here'; die;
    $layout = array(
      'key' => 'layout_5ea042b830db4',
      'name' => 'jimmy',
      'label' => 'Resources',
      'display' => 'block',
      'sub_fields' => array(
        array(
          'key' => 'field_5ea04624a44c7',
          'label' => 'Resources field',
          'name' => 'resources_field',
          'type' => 'text',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'acfe_permissions' => '',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
      ),
      'min' => '',
      'max' => '',
      'acfe_flexible_thumbnail' => false,
      'acfe_flexible_category' => false,
      'acfe_flexible_render_template' => false,
      'acfe_flexible_render_style' => false,
      'acfe_flexible_render_script' => false,
      'acfe_flexible_settings' => false,
      'acfe_flexible_settings_size' => 'medium',
    );
    // $field = acf_get_field('field_5e8c357d83716');
    // $field['layouts']['layout_5ea042b830db4'] = $layout;
    // acf_remove_local_field('field_5e8c357d83716');
    // echo '<pre>' . print_r($field,1) . '</pre>';
    // acf_add_local_field($field);
  //}

  endif;