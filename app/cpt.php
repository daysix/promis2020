<?php

namespace App;

/**
 * Register custom post types
 */

add_action('init', 
  function (){

    register_post_type(
      'mm',
      array(
        'labels' => array(
          'name' => __('Mega Menus'),
          'singular_name' => __('Mega Menu'),
          'menu_name'          => _x('Mega Menus', 'admin menu', 'your-plugin-textdomain'),
          'name_admin_bar'     => _x('Mega Menu', 'add new on admin bar', 'your-plugin-textdomain'),
          'add_new'            => _x('Add Mega Menu', 'book', 'your-plugin-textdomain'),
          'add_new_item'       => __('Add New Mega Menu', 'your-plugin-textdomain'),
          'new_item'           => __('New Mega Menu', 'your-plugin-textdomain'),
          'edit_item'          => __('Edit Mega Menu', 'your-plugin-textdomain'),
          'view_item'          => __('View Mega Menus', 'your-plugin-textdomain'),
          'all_items'          => __('All Mega Menus', 'your-plugin-textdomain'),
          'search_items'       => __('Search Mega Menus', 'your-plugin-textdomain'),
          'parent_item_colon'  => __('Parent Mega Menu:', 'your-plugin-textdomain'),
          'not_found'          => __('No Mega Menus found.', 'your-plugin-textdomain'),
          'not_found_in_trash' => __('No Mega Menus found in Trash.', 'your-plugin-textdomain')
        ),
        'hierarchical' => true,
        'description' => 'Mega Menus',
        'public' => true,
        'has_archive'        => true,
        'menu_icon' => 'dashicons-editor-kitchensink',
        'rewrite'            => array('slug' => 'mega-menu'),
        'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
      )
    );

      register_taxonomy(
        'audiences',
        array('resource'),
        array(
          'labels' => array(
            'name'							=> _x('Audience', 'taxonomy general name'),
            'singular_name'              	=> _x('Audience', 'taxonomy singular name'),
            'search_items'               	=> __('Search Audiences'),
            'popular_items'              	=> __('Popular Audiences'),
            'all_items'                  	=> __('All Audiences'),
            'parent_item'                	=> null,
            'parent_item_colon'          	=> null,
            'edit_item'                  	=> __('Edit Audience'),
            'update_item'                	=> __('Update Audience'),
            'add_new_item'               	=> __('Add New Audience'),
            'new_item_name'              	=> __('New Audience Name'),
            'separate_items_with_commas' 	=> __('Separate audience with commas'),
            'add_or_remove_items'        	=> __('Add or remove audiences'),
            'choose_from_most_used'      	=> __('Choose from the most used audiences'),
            'not_found'                  	=> __('No audience found.'),
            'menu_name'                  	=> __('Audiences'),
          ),
          'hierarchical' => true,
          'rewrite' => array('slug' => 'audiences'),
          'capabilities' => array(
              'manage__terms' => 'edit_posts',
              'edit_terms' => 'manage_categories',
              'delete_terms' => 'manage_categories',
              'assign_terms' => 'edit_posts'
          )
        )
      );

      register_post_type(
        'resource',
        array(
          'labels' => array(
            'name'         => __('Resources'),
            'singular_name'   => __('Resource'),
            'add_new'           => _x('Add New', 'resource'),
            'add_new_item'      => __('Add New Resources'),
            'edit_item'         => __('Edit Resources'),
            'new_item'          => __('New Resources'),
            'all_items'         => __('All Resources'),
            'view_item'         => __('View Resources'),
            'search_items'      => __('Search Resources'),
            'not_found'         => __('No resources found'),
            'not_found_in_trash' => __('No resources found in the Trash'),
            'parent_item_colon' => '',
            'menu_name'         => 'Resources'
          ),
          'taxonomies' => array('post_tag'),
          'hierarchical' => true,
          'description' => 'Resources',
          'public' => true,
          'has_archive' => true,
          'menu_icon' => 'dashicons-media-spreadsheet',
          'rewrite' => array('slug' => 'resource'),
          'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
        )
      );

      register_post_type(
        'area',
        array(
          'labels' => array(
            'name'         => __('Areas'),
            'singular_name'   => __('Area'),
            'add_new'           => _x('Add New', 'area'),
            'add_new_item'      => __('Add New Areas'),
            'edit_item'         => __('Edit Areas'),
            'new_item'          => __('New Areas'),
            'all_items'         => __('All Areas'),
            'view_item'         => __('View Areas'),
            'search_items'      => __('Search Areas'),
            'not_found'         => __('No areas found'),
            'not_found_in_trash' => __('No areas found in the Trash'),
            'parent_item_colon' => '',
            'menu_name'         => 'Areas'
          ),
          'taxonomies' => array('post_tag'),
          'hierarchical' => true,
          'description' => 'Areas',
          'public' => true,
          'has_archive' => true,
          'menu_icon' => 'dashicons-location-alt',
          'rewrite' => array('slug' => 'area'),
          'supports' => array('title', 'editor', 'excerpt')
        )
      );

      register_post_type(
        'region',
        array(
          'labels' => array(
            'name'         => __('Regions'),
            'singular_name'   => __('Region'),
            'add_new'           => _x('Add New', 'region'),
            'add_new_item'      => __('Add New Regions'),
            'edit_item'         => __('Edit Regions'),
            'new_item'          => __('New Regions'),
            'all_items'         => __('All Regions'),
            'view_item'         => __('View Regions'),
            'search_items'      => __('Search Regions'),
            'not_found'         => __('No regions found'),
            'not_found_in_trash' => __('No regions found in the Trash'),
            'parent_item_colon' => '',
            'menu_name'         => 'Regions'
          ),
          'taxonomies' => array('post_tag'),
          'hierarchical' => true,
          'description' => 'Regions',
          'public' => true,
          'has_archive' => true,
          'menu_icon' => 'dashicons-location-alt',
          'rewrite' => array('slug' => 'region'),
          'supports' => array('title', 'editor', 'excerpt')
        )
      );

      register_taxonomy(
        'resource-type',
        array('resource'),
        array(
          'labels' => array(
            'name'              => _x('Type', 'taxonomy general name'),
            'singular_name'                => _x('Type', 'taxonomy singular name'),
            'search_items'                 => __('Search Types'),
            'popular_items'                => __('Popular Types'),
            'all_items'                    => __('All Types'),
            'parent_item'                  => null,
            'parent_item_colon'            => null,
            'edit_item'                    => __('Edit Type'),
            'update_item'                  => __('Update Type'),
            'add_new_item'                 => __('Add New Type'),
            'new_item_name'                => __('New Type Name'),
            'separate_items_with_commas'   => __('Separate types with commas'),
            'add_or_remove_items'          => __('Add or remove types'),
            'choose_from_most_used'        => __('Choose from the most used types'),
            'not_found'                    => __('No types found.'),
            'menu_name'                    => __('Types'),
          ),
          'hierarchical' => true,
          'rewrite' => array('slug' => 'resource-type'),
          'capabilities' => array(
            'manage__terms' => 'edit_posts',
            'edit_terms' => 'manage_categories',
            'delete_terms' => 'manage_categories',
            'assign_terms' => 'edit_posts'
          )
        )
      );

      register_taxonomy(
        'resource-topic',
        array('resource'),
        array(
          'labels' => array(
            'name'              => _x('Topic', 'taxonomy general name'),
            'singular_name'                => _x('Topic', 'taxonomy singular name'),
            'search_items'                 => __('Search Topics'),
            'popular_items'                => __('Popular Topics'),
            'all_items'                    => __('All Topics'),
            'parent_item'                  => null,
            'parent_item_colon'            => null,
            'edit_item'                    => __('Edit Topic'),
            'update_item'                  => __('Update Topic'),
            'add_new_item'                 => __('Add New Topic'),
            'new_item_name'                => __('New Topic Name'),
            'separate_items_with_commas'   => __('Separate topics with commas'),
            'add_or_remove_items'          => __('Add or remove topics'),
            'choose_from_most_used'        => __('Choose from the most used topics'),
            'not_found'                    => __('No topics found.'),
            'menu_name'                    => __('Topics'),
          ),
          'hierarchical' => true,
          'rewrite' => array('slug' => 'resource-topic'),
          'capabilities' => array(
            'manage__terms' => 'edit_posts',
            'edit_terms' => 'manage_categories',
            'delete_terms' => 'manage_categories',
            'assign_terms' => 'edit_posts'
          )
        )
      );

      register_taxonomy_for_object_type('post_tag', 'resource-new');
      register_taxonomy_for_object_type('category', 'resource-new');
      
    }
  );
