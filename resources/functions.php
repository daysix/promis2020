<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__ . '/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin', 'cpt']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__) . '/config/assets.php',
            'theme' => require dirname(__DIR__) . '/config/theme.php',
            'view' => require dirname(__DIR__) . '/config/view.php',
        ]);
    }, true);

function create_nav_menu_object_tree($nav_menu_items_array)
{
    foreach ($nav_menu_items_array as $key => $value) {
        $value->children = array();
        $nav_menu_items_array[$key] = $value;
    }

    $nav_menu_levels = array();
    $index = 0;
    if (!empty($nav_menu_items_array)) do {
        if ($index == 0) {
            foreach ($nav_menu_items_array as $key => $obj) {
                if ($obj->menu_item_parent == 0) {
                    $nav_menu_levels[$index][] = $obj;
                    unset($nav_menu_items_array[$key]);
                }
            }
        } else {
            foreach ($nav_menu_items_array as $key => $obj) {
                if (in_array($obj->menu_item_parent, $last_level_ids)) {
                    $nav_menu_levels[$index][] = $obj;
                    unset($nav_menu_items_array[$key]);
                }
            }
        }
        $last_level_ids = wp_list_pluck($nav_menu_levels[$index], 'db_id');
        $index++;
    } while (!empty($nav_menu_items_array));

    $nav_menu_levels_reverse = array_reverse($nav_menu_levels);

    $nav_menu_tree_build = array();
    $index = 0;
    if (!empty($nav_menu_levels_reverse)) do {
        if (count($nav_menu_levels_reverse) == 1) {
            $nav_menu_tree_build = $nav_menu_levels_reverse;
        }
        $current_level = array_shift($nav_menu_levels_reverse);
        if (isset($nav_menu_levels_reverse[$index])) {
            $next_level = $nav_menu_levels_reverse[$index];
            foreach ($next_level as $nkey => $nval) {
                foreach ($current_level as $ckey => $cval) {
                    if ($nval->db_id == $cval->menu_item_parent) {
                        $nval->children[] = $cval;
                    }
                }
            }
        }
    } while (!empty($nav_menu_levels_reverse));

    $nav_menu_object_tree = $nav_menu_tree_build[0];
    return $nav_menu_object_tree;
}

function formatFilesize($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function includeWithVariables($filePath, $variables = array(), $print = true)
{
    $output = NULL;
    if(file_exists($filePath)){
        // Extract the variables to a local namespace
        extract($variables);

        // Start output buffering
        ob_start();

        // Include the template file
        include $filePath;

        // End buffering and return its contents
        $output = ob_get_clean();
    }
    if ($print) {
        print $output;
    }
    return $output;

}

function get_reading_time( $content, $override = '' ) {
    $wpm = 120;
    $wc = $content;
    $words = str_word_count(strip_tags($wc));
    $minutes = floor($words / $wpm);
    $seconds = floor($words % $wpm / ($wpm / 60));
    if( $override ) {
        $minutes = $override;
    }
    echo $minutes;
}

function get_categories_meta( $post, $taxonomy ) {

    $categories_list = '';
    $parent_category = '';

    $terms = get_the_terms( $post, $taxonomy );

    if($terms) {
        foreach ( $terms as $term ) {
            $categories_list .= $term->name . ', ';

            if($parent_category == '') {

                $parent = get_ancestors($term->term_id, $taxonomy);

                if($term->parent == 0) {
                    $t = $term->name;
                    $link = get_term_link( $term->term_id );
                } else {
                    $t = get_term( $parent[0] )->name;
                    $link = get_term_link( get_term( $parent[0] )->term_id );
                }

                $parent_category = '<a href="' . $link . '">' . htmlspecialchars_decode( $t ) . '</a>';
                        
            }

        }
        $categories_list = substr($categories_list, 0, -2);
    }

    return array(
        'parent' => $parent_category,
        'list' => $categories_list
    );
}

// custom include function
function include_template( $args ) {
	$params = shortcode_atts(array('template' => null), $args);
	if($params['template']){
		// echo get_template_part('parts/'.$params['template']); // non-blade version
		echo App\template('parts/' . $params['template']); // blade version
	}
}
add_shortcode( 'include', 'include_template' );

add_action( 'login_enqueue_scripts', 'admin_login_logo' );
function admin_login_logo() { ?> 
  <style type="text/css"> 
     body.login div#login h1 a{
     	background-image: url(<?php echo App\asset_path('images/logo.svg'); ?>);
     	background-size:230px;
     	padding-bottom: 20px;
     	width:auto;
     	height:50px;
     } 
  </style><?php 
 }

function promisFeaturedResources( $atts ) {

    $vars = shortcode_atts( array(
		'type' => 'individual',
    ), $atts );

    $output = ''; // '<h4 class="mb-0">Featured</h4>';
    
    switch( $vars['type'] ) {
        case 'individual':
        case 'manager':

            if( have_rows('featured-' . $vars['type'], 'option') ):
                while( have_rows('featured-' . $vars['type'], 'option') ) : the_row();

                    $resource = get_sub_field('resource');

                    $output .= '<p>
                        <a href="' . get_permalink($resource) . '"><strong>' . get_the_title($resource) . '</strong></a><br />';

                    if( get_sub_field('comment') ) { 
                        $output .= '<small>' . get_sub_field('comment') . '</small><br />';
                    }

                    if( get_sub_field('display_last_updated') == 'yes' ) {
                        $output .= '<small style="opacity:0.35">Last updated: ' . get_the_modified_date('d/m/Y', $resource) . '</small>';
                    }
                    $output .= '</p>';

                endwhile;
            endif;

            break;
    }

    if(isset($_GET['j'])) {
        return $output;
    } else {
        return '';
    }
}

function my_excerpt_length($length){
    return 30;
}
add_filter('excerpt_length', 'my_excerpt_length');

function develop_custom_excerpt_more($more) {
    global $post;
    return '...';
}
add_filter( 'excerpt_more', 'develop_custom_excerpt_more' );
    
add_shortcode('promis-featured-resources', 'promisFeaturedResources');

// Alternative
// Fully Disable Gutenberg editor.
add_filter('use_block_editor_for_post_type', '__return_false', 10);

// Don't load Gutenberg-related stylesheets.
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );
function remove_block_css() {
    wp_dequeue_style( 'wp-block-library' ); // WordPress core
    wp_dequeue_style( 'wp-block-library-theme' ); // WordPress core
    wp_dequeue_style( 'wc-block-style' ); // WooCommerce
    wp_dequeue_style( 'storefront-gutenberg-blocks' ); // Storefront theme
}


// acf drag n drop flexible layouts between repeaters
/* 
add_action('acf/input/admin_footer', function () {
    ?>
    <script type="text/javascript">

        (function($) {

            acf.add_action('ready', function($el){
                $(".values").sortable({
                    connectWith: ".values",
                    start: function(event, ui) {
                        acf.do_action('sortstart', ui.item, ui.placeholder);
                    },
                    stop: function(event, ui) {
                        acf.do_action('sortstop', ui.item, ui.placeholder);
                        $(this).find('.mce-tinymce').each(function() {
                            tinyMCE.execCommand('mceRemoveControl', true, $(this).attr('id'));
                            tinyMCE.execCommand('mceAddControl', true, $(this).attr('id'));
                        });
                    }
                });
            });

            acf.add_action('sortstop', function ($el) {

                // check if the dropped element is within a repeater field
                if($($el).parents('.acf-input > .acf-repeater').length) {

                    // get column_num from closest acf-row
                    var column_num = $($el).closest('.acf-row').attr('data-id');

                    // loop all (input) fields within dropped element and change / fix name
                    $($el).find('[name^="acf[field_"]').each(function() {
                        var field_name 		= 	$(this).attr('name');
                        field_name          =   field_name.match(/\[([a-zA-Z0-9_-]+\])/g); // split name attribute
                        field_name[1]       =   '[' + column_num + ']'; // set the new row name
                        var new_name        =   'acf' + field_name.join('');
                        $(this).attr('name', new_name);
                    });

                    // get closest flexible-content-field and loop all layouts within this flexible-content-field
                    $($el).closest('.acf-field.acf-field-flexible-content').find('.acf-input > .acf-flexible-content > .values > .layout').each(function(index) {

                        // update order number
                        $(this).find('.acf-fc-layout-order:first').html(index+1);

                        // loop all (input) fields within dropped element and change / fix name
                        $(this).find('[name^="acf[field_"]').each(function() {
                            var field_name 		= 	$(this).attr('name');
                            field_name          =   field_name.match(/\[([a-zA-Z0-9_-]+\])/g); // split name attribute
                            var tempIndex       =   parseInt(field_name[3].match(/([0-9]+)/g)); // hacky code
                            field_name[3]       =   field_name[3].replace(tempIndex, index); // set the new index
                            var new_name        =   'acf' + field_name.join('');
                            $(this).attr('name', new_name);
                        });

                        // click already selected buttons to trigger conditional logics
                        $(this).find('.acf-button-group label.selected').trigger('click');
                    });
                }
            });

        })(jQuery);

    </script>
    <?php
});

*/

//found a nifty function to log out php in the console

// function console_log($output, $with_script_tags = true) {
//     $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
// ');';
//     if ($with_script_tags) {
//         $js_code = '<script>' . $js_code . '</script>';
//     }
//     echo $js_code;
// }


//for fetching child categories
function get_sub_cats($term) {


    $childcategories = get_terms( $term->taxonomy, array(
        'parent'    => $term->term_id,
        'hide_empty' => false
    ) );

    return  $childcategories; 
  
}

function get_the_resource_icon($resource) {

    $types = get_the_terms($resource->ID, 'resource-type');

    if($types) {

        switch($types[0]->slug) {
            case "article":
                $icon = 'fa-file-alt';
                break;
            
            case "podcast":
                $icon = 'fa-podcast';
                break;

            case "webinar":
                $icon = 'fa-podcast';
                break;

            case "top-tips":
                $icon = 'fa-thumbs-up';
                break;

            case "video":
                $icon = 'fa-video';
                break;
        }

        return '
        <span class="icon-resource position-absolute fa-stack">
            <i class="fas fa-circle fa-stack-2x text-white"></i>
            <i class="fas ' . $icon . ' fa-stack-1x fa-inverse text-secondary"></i>
        </span>';

    }

}


//this is to fix the Appearance -> Customise -> error issue
add_filter('wp_get_nav_menu_items', 'my_wp_get_nav_menu_items', 10, 3);
function my_wp_get_nav_menu_items($items, $menu, $args) {
    foreach($items as $key => $item)
        $items[$key]->description = '';

    return $items;
}