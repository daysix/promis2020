export default {
  init() {

    $('.map path').click(function() {
      window.location.assign('/' + $(this).attr('id').toLowerCase());
    });

    $('button.national-services').click(function(e) {
      e.preventDefault();
      var elmnt = document.getElementById('national-services');
      elmnt.scrollIntoView({ behavior: 'smooth', block: 'center' });
    });

    $('.map path').tooltip();

    // $('.map path').tooltip({'container': 'body', 'placement': 'bottom', 'title': 'Goodbye'});

  },
};
