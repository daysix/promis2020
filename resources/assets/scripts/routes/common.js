/* eslint-disable */
// import Headroom from 'headroom.js/dist/headroom';

import Headroom from 'headroom.js/dist/headroom';
import 'headroom.js/dist/jQuery.headroom';

import 'bootstrap-select/dist/js/bootstrap-select.min';
import 'mmenu-js/dist/mmenu.polyfills';
import 'mmenu-js/dist/mmenu';

// import exitIntent from 'exit-intent';
// import Cookies from 'js-cookie';
import lity from 'lity/dist/lity';

export default {
  init() {

    var headroom = new Headroom(document.querySelector('header'));
    headroom.init();

    // $('.wellbeing-header').headroom();

    if ($('#mobile-menu').length ) {
      new Mmenu(document.querySelector('#mobile-menu'),{
        // slidingSubmenus: false,
        navbar: {
          "title": '&nbsp;'
        },
        "extensions": [
          "shadow-page",
          "position-right",
        ]
      });
    }

      // var removeExitIntent = exitIntent({
      //   threshold: 10,
      //   maxDisplays: 1,
      //   eventThrottle: 200,
      //   onExitIntent: () => {
      //     setTimeout(() => {
      //       if(Cookies.get('promis-feedback-modal') != 'yes'){
      //         $('#launch-exit-modal').click();
      //         Cookies.set('promis-feedback-modal', 'yes', { expires: 1, path: '/' });
      //       }
      //       Cookies.set('promis-feedback-modal', 'yes', { expires:1, path: '/' });
      //     }, 20000);
      //   }
      // });
    

    // removeExitIntent();

   $('#mobile-menu-toggle-simple').toggle(function(){
      // $('#menu-search').fadeOut("fast", "linear");
      $(this).addClass('open');
      $('#mobile-menu-simple').fadeIn("fast", "linear");
      $('body').css("position", "fixed");
    }, function(){
      headroom.init();
      $(this).removeClass('open');
      $('#mobile-menu-simple').fadeOut("fast", "linear"); 
      $('body').css("position", "relative");
    });

    $('.carousel').slick({
      arrows: false,
      dots: true,
      //autoplay:true,
      //autoplaySpeed:4000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      accessibility: false,
    });

    $('.carousel-footer').slick({
      arrows: false,
      dots: false,
      autoplay:true,
      autoplaySpeed:1500,
      infinite: true,
      slidesToShow: 8,
      slidesToScroll: 1,
      accessibility: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $('.carousel-hero').slick({
      arrows: false,
      dots: true,
      fade: true,
      autoplay:true,
      autoplaySpeed:4000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      accessibility: false,
    });

    $('.carousel-testimonials').slick({
      arrows: false,
      dots: false,
      autoplay: true,
      autoplaySpeed: 4000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    });
    
    $('.carousel-testimonials-navigation img').eq(0).addClass('active');
    
    $('.carousel-testimonials-navigation a').click(function (e) {
      e.preventDefault();
      var index = $(this).index('.carousel-testimonials-navigation a');
      $('.carousel-testimonials').slick('slickGoTo', index);
      $('.carousel-testimonials-navigation > div').removeClass('active');
      $('.carousel-testimonials-navigation > div').eq(index).addClass('active');
    });

    $('.carousel-testimonials').on('afterChange', function (event, slick, currentSlide) {
      $('.carousel-testimonials-navigation > div').removeClass('active');
      $('.carousel-testimonials-navigation > div').eq(currentSlide).addClass('active');
    });

    $('.selectpicker').selectpicker();

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

  },
};
