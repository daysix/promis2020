// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

import 'slick-carousel/slick/slick.min';

import 'custom-event-polyfill';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import findHelp from './routes/find-help';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  findHelp,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
