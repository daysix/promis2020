
<div class="card cursor h-100">

  <?php echo wp_get_attachment_image( get_sub_field('image'), 'full', false, array('class' => 'w-100 h-auto rounded-top', 'alt' => get_sub_field('title')) ); ?>

  <div class="card-body bg-gray-light rounded-bottom">

    <?php echo wp_get_attachment_image( get_sub_field('icon'), 'profile', false, array('class' => 'mb-4', 'alt' => get_sub_field('title')) ); ?>

    <h4 class="subtitle text-primary"> @php (the_sub_field('title')) </h4>
    <p class="text-primary">@php (the_sub_field('subtitle'))</p>

    @php (the_sub_field('content')) 

    @if (get_sub_field('date'))
    <div class="d-block mt-4">
      @php (the_sub_field('date'))
    </div>
    @endif 

    <div class="d-block">
      @if (get_sub_field('start_time'))
      <div class="d-inline">
        @php (the_sub_field('start_time')) - 
  
      </div>
      @endif 
  
      @if (get_sub_field('end_time'))
      <div class="d-inline">
        @php (the_sub_field('end_time'))
  
      </div>
      @endif 
    </div>

 



    @include('parts.buttons')

  </div>

</div>
