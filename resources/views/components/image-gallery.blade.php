
@php $images = get_sub_field('images') @endphp
@if ($images)

    <?php if(get_sub_field('type') == 'carousel') { ?>

      <div class="carousel">

        @foreach($images as $image)
          <div>
            <?php echo wp_get_attachment_image( $image, 'full', false, array('class' => 'w-100 h-auto', 'alt' => get_sub_field('title')) ); ?>
          </div>
        @endforeach

      </div>

    <?php } else { ?>

      <div class="row no-gutters gallery">
        @foreach($images as $image)
          <div class="col-6">
            <?php echo wp_get_attachment_image( $image, 'full', false, array('class' => 'w-100 h-auto', 'alt' => get_sub_field('title')) ); ?>
          </div>
        @endforeach
      </div>

    <?php } ?>

@endif

