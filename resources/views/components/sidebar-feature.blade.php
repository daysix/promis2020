
<div class="card _h-100">

  @if (get_sub_field('type') == 'image')
    <?php echo wp_get_attachment_image( get_sub_field('image'), 'full', false, array('class' => 'w-100 h-auto rounded-top', 'alt' => get_sub_field('title')) ); ?>
  @endif

  @if (get_sub_field('type') == 'film')
    <div class="film-wrapper">
      <?php echo do_shortcode('[arve url="' . get_sub_field('film') . '"]'); ?>
    </div>
  @endif

  <div class="card-body bg-gray-light rounded-bottom">

    <h4 class="text-primary"> @php (the_sub_field('heading')) </h4>

    @php (the_sub_field('description'))

    <p><a href="@php (the_sub_field('button_link')) ">@php (the_sub_field('button_text')) </a></p>

  </div>

</div>