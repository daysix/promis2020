@if (have_rows('items'))

  <div class="accordion" id="accordian">

    <?php $n = 1; ?>

    @while (have_rows('items'))
      @php (the_row())

      <?php $uid = uniqid('items'); ?>

      <div class="mb-3 position-relative">
        <div id="heading-{{ $n }}">
          <h3 class="h4 cursor text-primary pr-5 mb-0 clearfix" data-toggle="collapse" data-target="#item-{{ $uid }}" aria-expanded="false" aria-controls="item--{{ $uid }}" collapsed>
            {{ the_sub_field('title') }}
            <div class="accordian-control">
              <i class="h2 fas fa-plus-circle text-secondary"></i>
            </div>
          </h3>
        </div>
        <div id="item-{{ $uid }}" class="collapse" aria-labelledby="heading-{{ $uid }}" data-parent="#accordian">
          <div class="py-4 pr-5">

            {{ the_sub_field('content') }}

            @include('parts.buttons')

          </div>
        </div>
      </div>

      <?php $n++; ?>

    @endwhile

  </div>

@endif