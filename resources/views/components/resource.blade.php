@php
    $resource = get_sub_field('resource');
@endphp

{{-- @php
    setup_postdata($resource);
@endphp --}}








    @php
         $category_meta = get_categories_meta(get_post($resource->ID), 'resource-topic');
         
     @endphp
 
        <div class="mb-4">
 
            <a href="{{ get_the_permalink($resource->ID) }}">
               @if (get_post_thumbnail_id($resource->ID))
                   {!! wp_get_attachment_image(get_post_thumbnail_id($resource->ID), 'latest', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title', $resource->ID)]) !!}
               @else
                   <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3"
                       alt="{!! get_sub_field('title', $resource->ID) !!}">
               @endif
            </a>
 
             @if ($category_meta['parent'])
               <span class="meta">{!! $category_meta['parent'] !!}</span>
             @endif
             <h4 class="mb-1"><a href="{{ get_the_permalink($resource->ID) }}">{!! get_the_title($resource->ID) !!}</a></h4>
             <div class="meta mb-3">{{ get_reading_time(get_the_content($resource->ID)) }} min read</div>
             @php
                 $excerpt = get_the_excerpt($resource->ID);
                 $formatted = html_entity_decode($excerpt);
             @endphp
             {{-- <p>{{ get_the_excerpt($resource->ID) }}</p> --}}
             <p>{{ $formatted }}</p>
             {{-- <p>{{ the_excerpt() }}</p> --}}
 
        </div> 
         
 

 



@php wp_reset_postdata(); @endphp 