<div class="text-{{ the_sub_field('text_alignment') }} content">

  @php (the_sub_field('content'))

</div>