
<?php
      $testimonials = get_sub_field('testimonials'); 
      if($testimonials): ?>
<div class="text-center">

<div class="row justify-content-center carousel-testimonials-navigation">
 
  <?php foreach($testimonials as $testimonial): ?>
  
    <div class="d-inline mx-4">
      <a href="#" class="testimonial-icon">
        <?php echo wp_get_attachment_image( $testimonial['image'], 'profile', false, array('class' => 'rounded-circle', 'alt' => $testimonial['name']) ); ?>
      </a>
      <h6 class="mt-3 subtitle" class="text-muted"><?php echo $testimonial['name']; ?></h6>
      <p class="text-primary"><?php echo $testimonial['role']; ?></p>
    </div>
  
  <?php endforeach; ?>
  
</div>
          
<div class="carousel-testimonials mt-4">
  
  <?php foreach($testimonials as $testimonial): ?>
                        
    <div>
      <span class="h5"><?php echo $testimonial['testimonial']?></span>
    </div>
  <?php endforeach; ?>

</div>

</div>
  
<?php endif; ?>