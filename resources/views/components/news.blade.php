
<h2 class="mb-4 mb-md-5">{{ get_sub_field('heading') }}</h2>

<?php 

  $args = array(
    'post_type' => 'resource',
    'posts_per_page' => get_sub_field('number_to_display'),
    'tax_query' =>  array(
      array (
        'taxonomy' => 'resource-type',
        'field' => 'term_id',
        'terms' => get_sub_field('category')
      )
),
    'orderby'			=> 'date',
	  'order'				=> 'DESC'
  );
      
  $query = new WP_Query($args);
  $posts = $query->posts;

  // global $post;

  if($posts){ ?>

    <div class="row">

      <?php foreach($posts as $post){
         
        setup_postdata($post);

        //echo print_r($post, 1); ?>

        <div class="col-12 col-md-6 col-lg-4 mb-4 mb-md-5">

          <a href="<?php echo get_the_permalink($post->ID); ?>" class="no-hover">

          <div class="card cursor _h-100">

            @php 
            $backgroundurl = wp_get_attachment_image_src( get_field('resource-image', $post->ID));
            @endphp 

          <?php echo wp_get_attachment_image( get_field('resource-image', $post->ID), 'full', false, array('class' => 'w-100 h-auto rounded-top', 'alt' => get_sub_field('title')) ); ?>

          <div class="card-body bg-gray-light rounded-bottom">
              @php print_r( $backgroundurl ); @endphp
            <?php echo wp_get_attachment_image( get_field('icon'), 'profile', false, array('class' => 'mb-4', 'alt' => get_sub_field('title')) ); ?>

            <h4 class="mb-2 text-primary">{{ $post->post_title }} </h4>

            <p class="text-body"><?php echo get_the_excerpt($post->ID); ?></p>

            <a href="<?php echo get_the_permalink($post->ID); ?>" class="btn btn-secondary">View resources</a>

          </div>

        </div>

      </a>

      </div>

       <?php } ?>

      </div>
  <?php } ?>

