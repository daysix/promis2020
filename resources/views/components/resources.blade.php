
<h2 class="mb-4 mb-md-5">{{ get_sub_field('heading') }}</h2>

<?php 

  $args = array(
    'post_type' => 'resource',
    'posts_per_page' => get_sub_field('number_to_display'),
    'tax_query' =>  array(
      array (
        'taxonomy' => 'resource-topic',
        'field' => 'term_id',
        'terms' => get_sub_field('category')
      )
),
    'orderby'			=> 'date',
	  'order'				=> 'DESC'
  );
      
  $query = new WP_Query($args);
  $posts = $query->posts;

  // global $post;

  if($posts){ ?>

    <div class="row">

      <?php foreach($posts as $post){
         
        setup_postdata($post);

        //echo print_r($post, 1); ?>

        <div class="col-12 col-md-6 col-lg-4 mb-4 mb-md-5">

            @php
            $category_meta = get_categories_meta(get_post($post->ID), 'resource-topic');
            
        @endphp
    
           <div class="mb-4">
    
               <a href="{{ get_the_permalink($post->ID) }}">
                  @if (get_post_thumbnail_id($post->ID))
                      {!! wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'latest', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title', $post->ID)]) !!}
                  @else
                      <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3"
                          alt="{!! get_sub_field('title', $post->ID) !!}">
                  @endif
               </a>
    
                @if ($category_meta['parent'])
                  <span class="meta">{!! $category_meta['parent'] !!}</span>
                @endif
                <h4 class="mb-1"><a href="{{ get_the_permalink($post->ID) }}">{!! get_the_title($post->ID) !!}</a></h4>
                <div class="meta mb-3">{{ get_reading_time(get_the_content($post->ID)) }} min read</div>
                <p>{{ get_the_excerpt($post->ID) }}</p>
    
           </div> 
            

      </div>

       <?php } ?>

      </div>
  <?php } ?>

