<div class="text-{{ the_sub_field('text_alignment') }} content">

  @if (get_sub_field('heading'))
  <h2>
    @php (the_sub_field('heading'))
  </h2>
  @endif

  @php (the_sub_field('content'))

  @include('parts.buttons')

</div>