<?php echo wp_get_attachment_image( get_sub_field('image'), 'full', false, array('class' => 'w-100 h-auto', 'alt' => get_sub_field('title')) ); ?>
<small>{{ get_sub_field('caption') }}</small>