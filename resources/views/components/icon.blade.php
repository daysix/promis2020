<div class="mt-5 icon-wrapper">
    <?php echo wp_get_attachment_image( get_sub_field('icon'), 'profile', false, array('class' => 'float-left', 'alt' => get_sub_field('title')) ); ?>
    <div class="overflow-auto">

        @php (the_sub_field('text')); @endphp 

    </div>
</div>


