{{--
  Template Name: Projects Listing
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page-projects')
  @endwhile
@endsection
