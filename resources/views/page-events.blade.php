{{-- Template Name: Events Listing --}}

@extends('layouts.app')

@section('content')
    @while (have_posts()) @php the_post() @endphp

        {{-- @include('partials.content-page-events') --}}

        @php echo do_shortcode('[tribe_events]') @endphp

    @endwhile
@endsection
