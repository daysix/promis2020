<!doctype html>
<html {!! get_language_attributes() !!}>

@include('partials.head')

<body @php body_class() @endphp>

    <div id="page">

        @php do_action('get_header') @endphp

        @include('partials.header')

        @include('partials.hero')

        @yield('content')

        @php do_action('get_footer') @endphp

        @include('partials.footer')

    </div>

    @php wp_footer() @endphp

    @include('partials.mobile-menu')

</body>

</html>
