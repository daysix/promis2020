<div id="mobile-menu-simple" class="bg-primary text-white">
  <div class="d-flex" style="width:100vw; height:100vh;">
      <div class="align-self-center m-auto text-center">
          <?php
              wp_nav_menu(
                      array(
                          'menu' => 'Main menu',
                          'container' => false,
                          'menu_id' => 'main-navigation',
                          'menu_class' => false
                      )
                  );
          ?>
      </div>
  </div>
</div>