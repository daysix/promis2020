<? if(!empty( get_the_content())){ 
  $content_width = (get_field('content_width') ? $content_width = get_field('content_width') : 'container-lg');
?>
 
  <section>
    <div class="<?php echo $content_width; ?>">

    @if( have_rows('sidebar_components') )
      <div class="row">
        <div class="col-12 col-md-8">
          <div class="content <?php echo $content_width; ?>">
            @php(the_content())
          </div>
        </div>
        <div class="col-12 col-md-4">
          @include('parts.sidebar-components')
        </div>
      </div>
    @else
      <div class="content <?php echo $content_width; ?>">
        @php(the_content())
      </div>
    @endif

  </div>
  </section>

  
<?php } ?>

@include('parts.components')
