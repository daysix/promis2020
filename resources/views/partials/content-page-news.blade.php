@php

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $args = array(
        'posts_per_page' => 9,
        'paged' => $paged,
        'post_type'	=> 'post',
        'orderby' => 'publish_date',
        'order' => 'desc'
    );

    
    $query = new WP_Query($args);
    
@endphp 



@if(!empty( get_the_content())) 
  
  @php  $content_width = (get_field('content_width') ? $content_width = get_field('content_width') : 'container'); @endphp


<section class="bg-gray-light">
  <div class="content <?php echo $content_width; ?>">
    @php(the_content()) @endphp
    </div>
  </section>
  
@endif 

  <section class="bg-gray-light">
    <div class="container container-lg">
      @if(get_field('featured_news'))
        @php
            $post_id = (get_field('featured_news'));
            $img_url = get_the_post_thumbnail_url($post_id);
        @endphp

        <a href="{{ get_the_permalink($post_id) }}">
            <div class="feature">
              <div class="row no-gutters h-100">
                <div class="col-12 col-md-6 image" style="background-image: url('<?php echo $img_url ?> ')">
                </div>
                <div class="col-12 col-md-6"> 
                        <div> 
                          <div class="content-wrapper">
                            <div>
                              <p class="mb-4 d-inline-block">
                                  {!! get_the_date(get_option('date_format'), $post_id) !!} |
                              </p>
                              @if (get_the_terms(($post_id), 'category'))
                                <p class="d-inline-block"> 
                                @php
                                foreach (get_the_terms(($post_id), 'category') as $cat) {
                                  echo ($cat->name);
                                }
                                @endphp
                                </p> 
                            @endif 
                            </div>
                            <h1>{!! get_the_title($post_id) !!}</h1>
                          </div> 
                        </div> 
                      </div> 
                    </div>
                  </div>
                </a> 
        @endif 
    </div> 
</section> 


<section class="bg-gray-light">
  <div class="container-lg">

<?php if( $query->have_posts() ): ?>

        <div class="row">

            <?php while( $query->have_posts() ) : $query->the_post(); ?>

            <div class="col-md-4 col-sm-6 col-12 pb-5">              
                  @if(!get_the_post_thumbnail_url())
                  @php
                      $url = get_bloginfo('template_directory').'images/placeholder/16x9-placeholder-' . rand(1,5) . '.png';
                  @endphp
                  @else 
                  @php
                      $url = get_the_post_thumbnail_url();
                  @endphp
                  @endif 
            

                    <div class="card news-card">   
                      <div style="background-image: url('<?php echo esc_url( $url); ?> ')" class="news-image">
                      </div>       
                      <div class="card-news-content"> 
                        <h4 class="mb-1"><a href="{{ the_permalink() }}">{!! the_title() !!}</a></h4>
                        <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>
                        <p>{{ the_excerpt() }}</p>
                      </div>
                    </div>

            </div> 
                
            <?php endwhile; ?>
          </div> 
          
          <?php endif; ?>
          
          <p><?php wp_pagenavi( array( 'query' => $query )); ?></p>

    <?php wp_reset_query();	 // Restore global post data  ?>

  </div>
</section>