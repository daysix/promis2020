<footer>
    <div class="bg-gradient-blue text-center text-sm-left py-5">
        <div class="container-lg">
            <span class="h2 text-white">
                Let us know how we can improve the site to meet your needs
            </span>
            <a href="#exit-modal" class="launch-exit-modal btn btn-lg btn-secondary mt-4 mt-sm-0 float-sm-right"
                data-lity>Leave some feedback</a>
        </div>
    </div>

    @if (!is_front_page())

        <div class="bg-gray-light py-4">
            <div class="container-lg">
                <div class="row carousel-footer">

                    @if (have_rows('partners', 'option'))
                        @while (have_rows('partners', 'option'))
                            @php(the_row())
                            <div class="col text-center">
                                <?php echo wp_get_attachment_image(get_sub_field('logo'), 'full', false, ['class' => 'w-100 h-auto', 'alt' => get_sub_field('title')]); ?>
                            </div>
                        @endwhile
                    @endif

                </div>
            </div>
        </div>

    @else

        <section class="bg-gray-light">
            <div class="container-lg">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <div class="pr-0 pr-md-5 mb-5">
                            <h2>Working Together</h2>
                            <p>The National Wellbeing Hub is a partnership between national, local and professional
                                bodies with a shared passion for looking after the emotional and psychological wellbeing
                                of our country’s health and social services workers.</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">

                        @if (have_rows('partners', 'option'))
                            <div class="row">
                                @while (have_rows('partners', 'option'))
                                    @php(the_row())
                                    <div class="col-4 col-md-3 col-lg-2 text-center">
                                        <?php echo wp_get_attachment_image(get_sub_field('logo'), 'full', false, ['class' => 'w-100 h-auto mb-4', 'alt' => get_sub_field('title')]); ?>
                                    </div>
                                @endwhile
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>

    @endif

    <section>
        <div class="container-lg">
            <div class="row">
                <div class="col-12 col-lg-9 mb-4 mb-lg-0">
                    <div class="mr-0 mr-md-3">
                        {{ the_field('footer_about', 'option') }}
                    </div>

                    <a class="twitter-logo d-inline-block my-3" href="https://twitter.com/NatWellbeingHub"
                        target="_blank"><i class="fab fa-twitter"></i></a>

                </div>

                <div class="col-12 col-lg-3">
                    {{ the_field('footer_menu', 'option') }}
                </div>

            </div>
        </div>
    </section>
    <div class="container-lg">
        <p><small>Website by <a href="https://daysix.co" target="_blank">daysix</a></small></p>
    </div>
</footer>

<div id="exit-modal" class="lity-hide bg-white p-4 p-md-5 rounded overflow-auto">
    <div class="container-sm">
        <img src="@asset('images/logo.svg')" class="mb-4 mb-md-5">
        <p>Welcome to the Hub. We hope you’ll find the support you’re looking for. To help us improve the site and make
            it relevant to you, please take a minute to answer a few quick questions. Thank you.</p>
        <a href="https://www.surveymonkey.co.uk/r/D95VX7V" class="btn btn-md-lg btn-primary">Give us feedback</a>
        <button class="btn btn-link float-right" data-lity-close>No thanks</button>
    </div>
</div>
