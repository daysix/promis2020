<div id="map-container" class="pt-4 pt-md-5 bg-gray-light">
  <div class="container container-lg">
    <div class="row">
      <div class="col-12 col-md-7">
        <span class="meta text-secondary">Local support</span>
        <h1>{{ the_title() }}</h1>
        @php (the_content()) @endphp
      </div>

      <div class="col-12 col-md-5">
        <div class="bg-white rounded p-4 p-lg-5 mb-4">

          {{ the_field('contact_details') }}

        </div>
      </div>
    </div>
  </div>
</div>

<div class="d-md-none">
  <div class="container container-sm pt-4">
    @if (have_rows('areas'))
      @while ( have_rows('areas') ) @php the_row() @endphp
        @php $area = get_sub_field('area'); @endphp
        <h3 class="pb-4 mb-0"><a href="{{ get_the_permalink($area->ID) }}" class="d-block">{{ $area->post_title }}</a></h3>
      @endwhile
      @endif
  </div>
</div>

<section class="d-none d-md-block pt-0">

  <div class="bg-light pt-5">
    <div class="container container-lg">

      <nav class="area-tabs text-center">
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist" style="line-height:1.5">

          @if (have_rows('areas'))
            @while ( have_rows('areas') ) @php the_row() @endphp

              @php 
                $area = get_sub_field('area');
              @endphp

              <a class="nav-item nav-link py-4 <?php if( get_row_index() == 1 ){ ?>active<?php } ?>" id="nav-support-tab" data-toggle="tab" href="#tab-{{ $area->post_name }}" role="tab" aria-controls="nav-support" aria-selected="<?php if( get_row_index() == 1 ){ ?>true<?php }else{ ?>false<?php } ?>">
                <span class="h4">{{ get_the_title( $area->ID ) }}</span>
              </a>
            @endwhile
          @endif

        </div>
      </nav>

    </div>
  </div>

  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

    @if (have_rows('areas'))

        @while ( have_rows('areas') ) @php the_row() @endphp

          @php 
            $area = get_sub_field('area');
          @endphp

          <div class="tab-pane fade <?php if( get_row_index() == 1 ){ ?>show active<?php } ?>" id="tab-{{ $area->post_name }}" role="tabpanel" aria-labelledby="nav-learn-tab">
            <div class="container-lg py-5">

              <h3 class="h1">{{ $area->post_title }}</h3>

              {!! apply_filters('the_content', $area->post_content) !!}

            </div>
          </div>

        @endwhile

    @endif

  </div>

</section>

<section class="bg-gray-light">
  <div class="container container-lg">
    <div class="row">
      <div class="col-12 col-md-8">
        <h2>Latest on the hub</h2>

        @php

          $args = array(
              'posts_per_page' => get_option('posts_per_page'),
              'post_type'	=> 'resource',
              'posts_per_page' => 3
          );

          $query = new WP_Query($args);

        @endphp

        @if( $query->have_posts() )

            <div class="resource-container">

                <div class="mt-4">

                    @while( $query->have_posts() )
                      @php $query->the_post() @endphp

                      @php
                        $category_meta = get_categories_meta( get_post(), 'resource-topic' );
                      @endphp

                    <div class="row mb-5">
                      <div class="col-12 col-md-6">
                        
                        <a href="{{ the_permalink() }}">
                          @if (get_post_thumbnail_id())
                            {!! wp_get_attachment_image( get_post_thumbnail_id(), 'full', false, array('class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')) ); !!}
                          @else 
                            <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3" alt="{!! get_sub_field('title') !!}">
                          @endif
                        </a>

                      </div>
                      <div class="col-12 col-md-6">
                        @if ($category_meta['parent'])
                          <span class="meta">{!! $category_meta['parent'] !!}</span>
                        @endif
                        <h4 class="mb-1"><a href="{{ the_permalink() }}">{{ the_title() }}</a></h4>
                        <div class="meta mb-3">{{ get_reading_time( get_the_content() ) }} min read</div>
                        <p>{{ the_excerpt() }}</p>
                      </div>
                    </div>

                    @endwhile
                
                </div>

            </div>
          
        @endif

        @php wp_reset_postdata() @endphp

      </div>
      <div class="col-12 col-md-4">
        <h3>Most popular</h3>

        @php

        $popular_resources = pvc_get_most_viewed_posts( array(
          'order'	=> 'desc',
          'post_type' => array('resource'),
        ) );

        // echo '<pre> ' . print_r( $popular_resources, 1) . '</pre>';

        @endphp

        @if ($popular_resources)
          @foreach ($popular_resources as $post)

            @php 
              $category_meta = get_categories_meta( $post, 'resource-topic' );
            @endphp

            <div class="row no-gutters mb-5">
              <div class="col-4">
                

                <a href="{{ get_the_permalink($post->ID) }}">
                  @if (get_post_thumbnail_id( $post->ID ))
                    {!! wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), 'square', false, array('class' => 'w-100 h-auto rounded mb-3', 'alt' => get_the_title( $post->ID ))) !!}
                  @else 
                    <img src="@asset('images/placeholder/square-placeholder-' . rand(1,4) . '.png')" class="w-100 h-auto rounded mb-3" alt="{!! get_sub_field('title') !!}">
                  @endif
                </a>

              </div>
              <div class="col-8">
                <div class="ml-4">
                  @if ($category_meta['parent'])
                    <span class="meta">{!! $category_meta['parent'] !!}</span>
                  @endif
                  <h5 class="mb-1"><a href="{{ get_the_permalink( $post->ID) }}">{{ get_the_title( $post->ID ) }}</a></h5>
                </div>
              </div>
            </div>

          @endforeach
        @endif

      </div>
    </div>
  </div>

</section>

@include('parts.components')

