@php

$paged = get_query_var('paged') ? get_query_var('paged') : 1;

$category = get_queried_object();

$args = [
    'post_type' => 'resource',
    'posts_per_page' => 9,
    'paged' => $paged,
    'orderby' => 'publish_date',
    'order' => 'desc',
    'tax_query' => [
        [
            'taxonomy' => 'audiences',
            'field' => 'id',
            'terms' => $category->term_id,
        ],
    ],
];

$query = new WP_Query($args);

@endphp

<section>
    <div class="container-lg">
        <div class="row mb-5">

            @while ($query->have_posts()) @php $query->the_post() @endphp

                @php
                    $category_meta = get_categories_meta(get_post(), 'resource-topic');
                @endphp


                <div class="col-md-4 col-sm-6 col-12">

                    <a href="{{ the_permalink() }}">

                                                    @if (get_field('featured_film'))
                                                    {!! do_shortcode('[arve url="' . get_field('featured_film') . '" /]') !!}
                                                @elseif (get_post_thumbnail_id())
                                                    {!! wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')]) !!}
                                                @else
                                                    <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')"
                                                        class="w-100 h-auto rounded mb-3"
                                                        alt="{!! get_sub_field('title') !!}">
                                                @endif
                    </a>

                    @if ($category_meta['parent'])
                        <span class="meta">{!! $category_meta['parent'] !!}</span>
                    @endif
                    <h4 class="mb-1"><a href="{{ the_permalink() }}">{{ the_title() }}</a></h4>
                    <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>
                    <p>{{ the_excerpt() }}</p>

                    <a href="<?php the_permalink(); ?>"
                        class="d-inline-block mb-4 mb-md-5">Read more</a>

                </div>


            @endwhile
        </div>

        <p><?php wp_pagenavi(['query' => $query]); ?></p>

    </div>
</section>
