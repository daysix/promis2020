<?php 

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $args = array(
        'posts_per_page' => 9,
        'paged' => $paged,
        'post_type'	=> 'resource',
        'orderby' => 'publish_date',
        'order' => 'desc'
    );

    if($_GET['term']){
        $args['s'] = $_GET['term'];
    }

    if(!empty($_GET['type']) || !empty($_GET['topics'])){

        $filter['tax_query'] = array();
       
        if(!empty($_GET['type'])){
            array_push(
                $filter['tax_query'],
                array(
                    'taxonomy' => 'resource-type',
                    'field'    => 'term_id',
                    'terms'    => $_GET['type']

                )
            );
        }
        
        if(!empty($_GET['topics'])){
            array_push(
                $filter['tax_query'],
                array(
                    'taxonomy' => 'resource-topic',
                    'field'    => 'term_id',
                    'terms'    => $_GET['topics']
                )
            );
        }

        if(count($filter['tax_query']) > 1){
            $filter['tax_query']['relation'] = 'AND';
        }

        $args = array_merge($filter, $args);
        
    } ?>


    
    <?php
   
    $query = new WP_Query($args);
    
    ?>



<? if(!empty( get_the_content())){ 
  $content_width = (get_field('content_width') ? $content_width = get_field('content_width') : 'container');
  ?>

<section class="bg-white">
  <div class="content <?php echo $content_width; ?>">
    @php(the_content()) @endphp
    </div>
  </section>
  
  <?php } ?>

  <section class="bg-gray-light py-4 pt-5">
    <div class="container-lg resource-filters pb-3 pb-md-0">

        <div class="h2 mb-4">Not sure what you’re looking for? Filter...</div>

        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="get" id="resource-filter" class="form-group-lg">
          <input type="hidden" value="1" name="paged">
            
            <div class="row mb-4">
                
                <div class="col-12 col-sm-4">
                   
                    <span class="meta text-primary d-inline-block mb-1 mb-md-2">Resource type</span><br />

                      <?php 


                        $terms = get_terms([
                            'taxonomy' => 'resource-type',
                            'hide_empty' => true
                          ]); 

                      ?>

                      <select name="type[]" class="selectpicker show-tick mb-4"  multiple data-style="btn-white" data-width="100%">  
                        <?php foreach($terms as $term) { ?>
                          <option value="<?php echo $term->term_id; ?>"  
                            <?php if($_GET['type'] && in_array($term->term_id, $_GET['type'])){ ?>
                              selected
                            <?php } ?>
                          >
                            <?php echo $term->name; ?>
                        </option>
                        <?php } ?>
                       
                        </select>

                       
                </div>

                <div class="col-12 col-sm-4 pr-4">
                
                  <span class="meta text-primary d-inline-block mb-1 mb-md-2">Resource topic</span><br />
                  
                  <?php 

                    $terms = get_terms([
                      'taxonomy' => 'resource-topic',
                      'hide_empty' => true
                    ]); 

                  ?>

                  <select name="topics[]" class="selectpicker show-tick mb-4"  multiple data-style="btn-white" data-width="100%"> 
                    <?php foreach($terms as $term) { ?>
                      <option value="<?php echo $term->term_id; ?>" <?php if($_GET['topics'] && in_array($term->term_id, $_GET['topics'])){ ?>selected<?php } ?>><?php echo $term->name; ?></option>
                    <?php } ?>
                  </select>
                

                </div>

                <div class="col-12 col-sm-4 ml-auto">
                  <span class="d-none d-md-block">&nbsp;<br /></span>
                  
                      <input type="submit" class="btn btn-primary" value="Filter">
                      
                      <a href="/resources" class="btn btn-link"><span class="_meta text-primary">Reset</span></a>
 
                  </div>

                </div>
            </div>
            
        </form>

    </div>
  </section>

<section>
  <div class="container-lg">

<?php if( $query->have_posts() ): ?>

        <div class="row mt-4">

            <?php while( $query->have_posts() ) : $query->the_post(); ?>

           @php
                $category_meta = get_categories_meta(get_post(), 'resource-topic');
                
            @endphp

            <div class="col-md-4 col-sm-6 col-12 mb-4">

                    <a href="{{ the_permalink() }}">
                      @if (get_post_thumbnail_id())
                          {!! wp_get_attachment_image(get_post_thumbnail_id(), 'latest', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')]) !!}
                      @else
                          <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3"
                              alt="{!! get_sub_field('title') !!}">
                      @endif
                  </a>

                    @if ($category_meta['parent'])
                      <span class="meta">{!! $category_meta['parent'] !!}</span>
                    @endif
                    <h4 class="mb-1"><a href="{{ the_permalink() }}">{!! the_title() !!}</a></h4>
                    <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>
                    <p>{{ the_excerpt() }}</p>

                </div> 
                
            <?php endwhile; ?>

            
          </div> 
          
          <?php endif; ?>
          
          <p><?php wp_pagenavi( array( 'query' => $query )); ?></p>

    <?php wp_reset_query();	 // Restore global post data  ?>

  </div>
</section>