@if (!empty(get_the_content()))

    @php  $content_width = (get_field('content_width') ? $content_width = get_field('content_width') : 'container'); @endphp


    <section class="bg-gray-light">
        <div class="content <?php echo $content_width; ?>">
            @php(the_content()) @endphp
        </div>
    </section>

@endif

@php echo do_shortcode('[tribe_events]') @endphp
