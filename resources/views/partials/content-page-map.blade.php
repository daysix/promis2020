
<div id="map-container" class="pt-4 pt-md-5 bg-gray-light">
  <div class="container container-lg">
    <div class="row">
      <div class="col-12 col-md-7">

        <div class="map-content pt-2 pt-md-0 positon">
          <h1>Local Services</h1>
          <p>Select your area for information<br />on local services:</p>
        </div>
        <div class="position-relative pt-3">
        
        <div class="map-title">
          <button class="national-services btn btn-sm btn-primary d-md-none">National Services</button>
        </div>
        <div class="_map-wrapper">
          @svg('map', 'map mb-5 w-100', ['title' => 'Map of Scotland'])
        </div>

      </div>

      </div>
      <div class="col-12 col-md-5" id="national-services">
        <div class="bg-white rounded p-4 p-lg-5 mb-4">

          {{-- <h4>National and local helplines</h4>

          <p>There are national and local helplines you can call for confidential support and advice.</p>  --}}

          @php the_content() @endphp

            {{-- <h4>Quick reference</h4>
            <p>
              <strong>National Wellbeing Helpline</strong> for everyone working in health &amp; social care services <a href="tel:111">Tel: 0800 111 4191</a>
            </p>


            <p>
              <strong>The Workforce Specialist Service:</strong> <a href="/the-workforce-specialist-service-wss/">more info</a>
            </p>

            <p>
              Breathing Space (lines open 6pm-2am weekdays, 24hrs weekend) <a href="tel:0800 83 85 87">Tel: 0800 83 85 87</a>
            </p>

            <p>
              NHS Inform (book a test) <a href="tel:0800 83 85 87">Tel: 0800 028 2816</a>
            </p>

            <p>
              NHS Inform: <a href="https://www.nhsinform.scot/">www.NHSinform.scot
            </p>

            <p>
            </a>Stay active and well: <a href="https://clearyourhead.scot/">ClearYourHead.scot</a>
            </p> --}}
  
            <div class="pt-5">

              <div class="d-flex mb-2">
                @svg('national_union', 'mr-3')
                <h4><a class="" href="/national-services">Other National Services and Unions</a></h4> 
              </div>
              <div class="d-flex mb-2">
                @svg('professional', 'mr-3')
                <h4><a class="" href="http://www.promis.scot/professional-groups/">Support by Professional Group</h4></a>
              </div>
              <div class="d-flex mb-2">
                @svg('third_sector', 'mr-3')
                <h4><a class="" href="http://www.promis.scot/organisations/">Third Sector Staff Support</h4> </a>
              </div>
                

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('parts.components')
