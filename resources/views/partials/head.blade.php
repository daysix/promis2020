<head>
  <!-- Hotjar Tracking Code for www.promis.scot -->
  <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1914568,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="180x180" href="@asset('images/favicon/apple-touch-icon.png')">
  <link rel="icon" type="image/png" sizes="32x32" href="@asset('images/favicon/favicon-32x32.png')">
  <link rel="icon" type="image/png" sizes="16x16" href="@asset('images/favicon/favicon-16x16.png')">
  {{-- <link rel="manifest" href="@asset('images/favicon/site.webmanifest')"> --}}
  <link rel="mask-icon" href="@asset('images/favicon/safari-pinned-tab.svg')" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#2d89ef">
  <meta name="theme-color" content="#ffffff">
  <meta name="format-detection" content="telephone=no">
  <meta property="og:image" content="<?php if(get_field('resource-image')) { echo wp_get_attachment_image_src(get_field('resource-image'), 'full')[0]; } else { the_field('default_social_image', 'option'); } ?>" />
  @php wp_head() @endphp
</head>
