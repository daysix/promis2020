<section class="bg-gray-light">
    <div class="container-lg">

        <div class="row">
            <div class="col-12 col-md-8 content">

                <h1 class="mb-1">{{ the_title() }}</h1>
                <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>

                {{-- @if (get_field('featured_film', $resource->ID))
                    {!! do_shortcode('[arve url="' . get_field('featured_film', $resource->ID) . '" /]') !!}
                @elseif (get_post_thumbnail_id($resource->ID))
                    {!! wp_get_attachment_image(get_post_thumbnail_id(), 'single', false, ['class' => 'w-100 h-auto rounded mb-3 mb-md-5', 'alt' => get_the_title()]) !!}
                @else

                @endif --}}

                @php the_content() @endphp

                <div class="exclude-section-padding">
                    @include('parts.components')
                </div>
            </div>
            <div class="col-12 col-md-4">

                @include('parts.sidebar-components')

                @php
                    
                    $tags = [];
                    $tag_data = get_the_tags(get_queried_object()->ID);

                    if($tag_data) {
                      foreach ($tag_data as $key => $tag) {
                          array_push($tags, $tag->term_id);
                      }
                    }
                    
                    $args = [
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'orderby' => 'publish_date',
                        'order' => 'desc',
                    ];
                    
                    $query = new WP_Query($args);
                    
                @endphp

                @if ($query->have_posts())

                    <h2 class="mb-4 mb-md-5">Latest news</h2>

                    <div class="resource-container">

                        <div class="mt-4">

                            @while ($query->have_posts())
                                @php $query->the_post() @endphp

                                @php
                                    $category_meta = get_categories_meta(get_post(), 'resource-topic');
                                @endphp

                                <div class="row mb-3 mb-md-4">
                                    <div class="col-12">

                                        <a href="{{ the_permalink() }}"> 
                                            {{-- @if (get_field('featured_film'))
                                                {!! do_shortcode('[arve url="' . get_field('featured_film') . '" /]') !!} --}}
                                            @if (get_post_thumbnail_id())
                                                {!! wp_get_attachment_image(get_post_thumbnail_id(), 'latest', false, ['class' => 'latest w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')]) !!}
                                            @else
                                                <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')"
                                                    class="w-100 h-auto rounded mb-3"
                                                    alt="{!! get_sub_field('title') !!}">
                                            @endif
                                        </a>

                                    </div>
                                    <div class="col-12">
                                        @if ($category_meta['parent'])
                                            <span class="meta">{!! $category_meta['parent'] !!}</span>
                                        @endif
                                        <h5 class="mb-1"><a href="{{ the_permalink() }}">{!! the_title() !!}</a></h5>
                                        <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read
                                        </div>

                                    </div>
                                </div>

                            @endwhile

                        </div>

                    </div>

                @endif

                @php wp_reset_postdata() @endphp

                @php
                    
                    $category = get_queried_object();
                    
                    $popular_resources = pvc_get_most_viewed_posts([
                        'post_type' => ['resource'],
                        'posts_per_page' => 5,
                        'order' => 'desc'
                    ]);
                    
                @endphp

                @if ($popular_resources)

                    <h2 class="mb-4 mb-md-5">Most popular resources</h2>

                    @foreach ($popular_resources as $post)

                        @php
                            $category_meta = get_categories_meta($post, 'resource-topic');
                        @endphp

                        <div class="row no-gutters mb-4">
                            <div class="col-4">

                                <a href="{{ get_the_permalink($post->ID) }}">
                                    @if (get_post_thumbnail_id($post->ID))
                                        {!! wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'square', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_the_title($post->ID)]) !!}
                                    @else
                                        <img src="@asset('images/placeholder/square-placeholder-' . rand(1,4) . '.png')" class="w-100 h-auto rounded mb-3"
                                            alt="{!! get_sub_field('title') !!}">
                                    @endif
                                </a>

                            </div>
                            <div class="col-8">
                                <div class="ml-4">
                                    @if ($category_meta['parent'])
                                        <span class="meta">{!! $category_meta['parent'] !!}</span>
                                    @endif
                                    <h5 class="mb-1"><a
                                            href="{{ get_the_permalink($post->ID) }}">{!! get_the_title($post->ID) !!}</a>
                                    </h5>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif

            </div>
        </div>

    </div>
</section>

@if ($resource_type)

    <section class="bg-white">
        <div class="container-lg">
            <h2 class="mb-4 mb-md-5">Other resources for {{ $related_resources_title }}</h1>
                @include('partials.related-resources')
        </div>
    </section>

@endif
