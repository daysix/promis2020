<header class="wellbeing-header"
  <?php $banner_enabled = (get_field('banner-enabled', 'option')) ? ' banner-enabled' : ''; ?>
  <?php $visitors_active = (get_users_browsing_site() >= '3') ? ' visitors-active' : ''; ?>

  <?php if (get_field('hero_type') == "hero_none" || is_single()) { ?>
      class="no-animate<?php echo $banner_enabled . $visitors_active ?>"
    <?php } else { ?>
      class="animate<?php echo $banner_enabled . $visitors_active ?>"
    <?php } ?>>

@include('partials.banner')

<div class="container-lg">

  <a href="<?php echo site_url(); ?>" class="logo-container">
    <img src="@asset('images/logo.svg')" class="logo">
  </a>

<nav class="main-nav text-primary main-nav-mega-menu" style="padding-right: 80px;">
    <ul>
      <?php if ($menu_items = create_nav_menu_object_tree(wp_get_nav_menu_items('Main menu'))) {
        foreach ($menu_items as $menu_item) { 
          if(!(get_field('mobile_only', $menu_item))) { ?>
          <li class="has-megamenu" data-content="<?php echo sanitize_title($menu_item->title); ?>">
            <a href="<?php echo $menu_item->url; ?>" class="
              <?php if(get_field('mega_menu', $menu_item)){ ?>
                mega-menu-dropdown-icon
              <?php } ?>
              "><?php echo $menu_item->title; ?></a>

            <?php if(get_field('mega_menu', $menu_item)){ ?>
            <div class="mega-menu-dropdown bg-gray-light">
              <div></div>
              <div class="container-lg">
                  @include('parts.mega-menu-components')
              </div>
              <div class="py-4">
                <div class="container-lg">
                  <a href="/resources" class="btn btn-secondary text-white">View all resources +</a>
                </div>
              </div>
            </div>
            <?php } ?>

          </li>
          <?php }
        }
      }
      ?>
    </ul>
  </nav>

  <div id="search-icon" class="d-lg-none">
    <a href="/search">
      <i class="far fa-search text-primary cursor"></i>
    </a>
  </div>

  <div id="search" class="d-none d-lg-block">
    <form action="/search/" method="get">
    {{-- <input type="text" name="q" class="form-control form-control-sm bg-gray-light border-0 pl-3" id="" aria-describedby="search" placeholder="Search..."> --}}
    <button type="submit" class="btn btn-sm btn-white"><i class="fas fa-search text-primary"></i></button>
    </form>
  </div>

      <a href="#mobile-menu" id="mobile-menu-toggle" class="mobile-menu-toggle d-lg-none">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </a>

  </div>
</header>
