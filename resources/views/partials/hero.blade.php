<div class="hero-container">

    @if (is_archive())
        @include('partials.hero-archive')
    @elseif (get_field('hero_type') && (get_field('hero_type') !== 'none'))
        @include('partials.hero-'.get_field('hero_type'))
    @endif

</div>

