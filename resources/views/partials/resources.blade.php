@if ($query->have_posts())

    <div class="resource-container">

        <div class="mt-4">

            @while ($query->have_posts())
                @php $query->the_post() @endphp

                @php
                    $category_meta = get_categories_meta(get_post(), 'resource-topic');
                @endphp

                <div class="row mb-5">
                    <div class="col-12 col-md-6">

                        <a href="{{ the_permalink() }}">
                            @if (get_post_thumbnail_id())
                                {!! wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')]) !!}
                            @else
                                <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3"
                                    alt="{!! get_sub_field('title') !!}">
                            @endif
                        </a>

                    </div>
                    <div class="col-12 col-md-6">
                        @if ($category_meta['parent'])
                            <span class="meta">{!! $category_meta['parent'] !!}</span>
                        @endif
                        <h4 class="mb-1"><a href="{{ the_permalink() }}">{{ the_title() }}</a></h4>
                        <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>
                        <p>{{ the_excerpt() }}</p>
                    </div>
                </div>

            @endwhile

        </div>

    </div>

@endif

@php wp_reset_postdata() @endphp
