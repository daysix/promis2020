<?php if( have_rows('hero_carousel') ): ?>

<div class="hero overflow-hidden <?php if(count(get_field('hero_carousel')) > 1) { ?>carousel-hero<?php } ?>">

    <?php while( have_rows('hero_carousel') ): the_row(); 
        $image = get_sub_field('image');
        ?>

    <div class="bg-gradient-blue">
        <div class="position-relative">

            <div class="hero-content hero-content-home hero-padding">
                <div>
                    <h1>{{ the_sub_field('title') }}</h1>
                    <p>{{ the_sub_field('subtitle') }}</p>
                    @if (get_sub_field('button_link') && get_sub_field('button_text'))
                        <p><a href="<?php the_sub_field('button_link'); ?>" class="btn btn-white">{{ the_sub_field('button_text') }}
                                +</a></p>
                    @endif

                    @if (get_sub_field('include_search'))


                        <div class="position-relative mt-3" style="max-width: 350px">

                            <?php
                            $new_term = stripslashes_deep($_GET['term']);
                            ?>
                            <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="get" id="resource-filter" class="form-group-lg">
                                <input type="text" name="term" id="term" value="<?php echo $new_term; ?>"
                                    placeholder="Search the wellbeing hub..."
                                    class="form-control bg-white border-0 w-100"
                                    style="border-radius:150px; padding-left: 45px;">
                                <i class="fas fa-search text-primary position-absolute" style="left:15px; top:15px"></i>
                                <a href="/resources" class="btn btn-link">

                                    <span id="term_cancel" <?php if(!$_GET['term']){ ?>style="visibility: hidden"
                                        <?php } ?>><i
                                            class="fas fa-times-circle text-primary position-absolute"
                                            style="top: 15px; right: 15px;"></i></span>

                                </a>
                            </form>
                        </div>

                    @endif

                </div>


            </div>

            <!-- Mask top trimmed (for larger sizes) -->
            <div class="hero-mask-container d-none d-xl-block">
                <img src="@asset('images/hero-mask-blank-985x415.png')" class="h-100">
                <img src="@asset('images/hero-mask-large.png')" class="h-100 hero-mask">
                <div class="hero-image hero-image-large" style="background-image: url({{ $image }});"></div>
            </div>

            <!-- Up -->
            <div class="hero-mask-container d-none d-md-block d-xl-none">
                <img src="@asset('images/hero-mask-blank-1082x456.png')" class="h-100">
                <img src="@asset('images/hero-mask-medium.png')" class="h-100 hero-mask">
                <div class="hero-image hero-image-medium" style="background-image: url({{ $image }});"></div>
            </div>

            <!-- Mobile -->
            <div class="d-md-none">
                <img src="@asset('images/hero-mask-mobile.png')" class="w-100 hero-mask hero-mask-mobile">
                <div class="hero-image hero-image-mobile" style="background-image: url({{ $image }}); ">
                    <img class="w-100"
                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAABCAQAAABeK7cBAAAAC0lEQVR42mNkAAIAAAoAAv/lxKUAAAAASUVORK5CYII=">
                </div>
            </div>

        </div>

    </div>

    <?php endwhile; ?>


</div>

<?php endif; ?>
