<section>
    <div class="container-lg">

        <div class="row">
            <div class="col-12 col-md-8">

                <h1 class="mb-4 mb-md-5">Latest</h1>

                @while (have_posts()) @php the_post() @endphp

                    @php
                        $category_meta = get_categories_meta(get_post(), 'resource-topic');
                    @endphp

                    <div class="row mb-5">
                        <div class="col-12 col-md-6">

                            <a href="{{ the_permalink() }}"> 
                                @if (get_field('featured_film'))
                                    {!! do_shortcode('[arve url="' . get_field('featured_film') . '" /]') !!}
                                @elseif (get_post_thumbnail_id())
                                    {!! wp_get_attachment_image(get_post_thumbnail_id(), 'full', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')]) !!}
                                @else
                                    <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')"
                                        class="w-100 h-auto rounded mb-3"
                                        alt="{!! get_sub_field('title') !!}">
                                @endif
                            </a>

                        </div>
                        <div class="col-12 col-md-6">
                            @if ($category_meta['parent'])
                                <span class="meta">{!! $category_meta['parent'] !!}</span>
                            @endif
                            <h4 class="mb-1"><a href="{{ the_permalink() }}">{{ the_title() }}</a></h4>
                            <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>
                            <p>{{ the_excerpt() }}</p>
                        </div>
                    </div>


                @endwhile


               {{-- @php
                $term = get_queried_object();
                $subcats = get_sub_cats($term);
                @endphp  --}}

                <p><?php wp_pagenavi(); ?></p>

            </div>
            <div class="col-12 col-md-4">

                <h2 class="mb-4 mb-md-5">Search</h2>

                <form action="/search/" method="get" class="topic-search-form mb-5">
                    <div class="position-relative mb-3">
                        <div style="margin-right: 50px;">
                            <input type="text" name="q" class="bg-gray-light form-control rounded px-4" id="topic-search" aria-describedby="topic-search" placeholder="Search">
                        </div>
                        <button type="submit" class="position-absolute btn btn-sm btn-secondary"><i class="fas fa-search text-white"></i></button>
                    </div>
                </form>

                @php
                $term = get_queried_object();
                $subcats = get_sub_cats($term);
                @endphp 


                @if  ($term->parent == 0)

                    <h3> Related areas </h3>

                    <div class="tags mt-3 mb-4">

                        @foreach($subcats as $subcat)
                        @php 
                        $url = "/" . $subcat->taxonomy . "/" . $subcat->slug

                        @endphp

                        <a href="{{ $url }}" class="btn btn-secondary related-area"> {{$subcat->name }} </a>

                        @endforeach

                    </div>
            
                @endif 



                @php
                    
                    $category = get_queried_object();
                    
                    $popular_resources = pvc_get_most_viewed_posts([
                        'order' => 'desc',
                        'post_type' => ['resource'],
                        'tax_query' => [
                            [
                                'taxonomy' => 'resource-topic',
                                'field' => 'id',
                                'terms' => $category->term_id,
                            ],
                        ],
                    ]);
                    
                @endphp

                @if ($popular_resources)

                    <h2 class="mb-4 mb-md-5">Most popular</h2>

                    @foreach ($popular_resources as $post)

                        @php
                            $category_meta = get_categories_meta($post, 'resource-topic');
                        @endphp

                        <div class="row no-gutters mb-5">
                            <div class="col-4">

                                <a href="{{ get_the_permalink($post->ID) }}">
                                    @if (get_field('featured_film', $post->ID))
                                        {!! do_shortcode('[arve url="' . get_field('featured_film', $post->ID) . '" /]') !!}
                                    @elseif (get_post_thumbnail_id($post->ID))
                                        {!! wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'square', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_the_title($post->ID)]) !!}
                                    @else
                                        <img src="@asset('images/placeholder/square-placeholder-' . rand(1,4) . '.png')" class="w-100 h-auto rounded mb-3"
                                            alt="{!! get_sub_field('title') !!}">
                                    @endif
                                </a>

                            </div>
                            <div class="col-8">
                                <div class="ml-4">
                                    @if ($category_meta['parent'])
                                        <span class="meta">{!! $category_meta['parent'] !!}</span>
                                    @endif
                                    <h5 class="mb-1"><a
                                            href="{{ get_the_permalink($post->ID) }}">{!! get_the_title($post->ID) !!}</a>
                                    </h5>
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif

                @include('parts.sidebar-components')

            </div>
        </div>

    </div>
</section>
