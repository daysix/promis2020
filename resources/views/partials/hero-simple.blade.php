<div class="hero hero-short overlay overlay-gradient bg bg-primary" style="background-image: url(<?php echo $hero_image; ?>)">
  
  <div>

      <div class="container-lg">

          <div class="hero-content">

              <div class="row">
                      
                  <div class="col-12 col-md-6 my-auto">

                      <div class="hero-feature-content highlight-left h-auto">

                          <span>{{ the_field('hero_pre_title') }}</span>
                          <h1 class="mb-0">{{ the_field('hero_title') }}</h1>

                      </div>

                  </div>

              </div>
                  
          </div>

      </div>

  </div>

</div>
