<?php 

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $args = array(
        'posts_per_page' => 9,
        'paged' => $paged,
        'post_type'	=> 'resource',
        'orderby' => 'publish_date',
        'order' => 'desc'
    );

    if($_POST['term']){
        $args['s'] = $_POST['term'];
    }

    if(!empty($_POST['type']) || !empty($_POST['topics'])){

        $filter['tax_query'] = array();
       
        if(!empty($_POST['type'])){
            array_push(
                $filter['tax_query'],
                array(
                    'taxonomy' => 'resource-type',
                    'field'    => 'term_id',
                    'terms'    => $_POST['type'],
                )
            );
        }
        
        if(!empty($_POST['topics'])){
            array_push(
                $filter['tax_query'],
                array(
                    'taxonomy' => 'resource-topic',
                    'field'    => 'term_id',
                    'terms'    => $_POST['topics'],
                )
            );
        }

        if(count($filter['tax_query']) > 1){
            $filter['tax_query']['relation'] = 'OR';
        }

        $args = array_merge($filter, $args);
        
    } ?>

    <!--<?php echo '<pre>' . print_r($_GET) . '</pre>'; ?> --> 
    <!--<?php foreach ($_GET as $key => $value) {
    echo $key . ' => ' . $value . '<br />';
} ?> --> 
    
    <?php
   
    $query = new WP_Query($args);
    
    ?>



<? if(!empty( get_the_content())){ 
  $content_width = (get_field('content_width') ? $content_width = get_field('content_width') : 'container');
  ?>

<section class="bg-white">
  <div class="content <?php echo $content_width; ?>">
    @php(the_content()) @endphp
    </div>
  </section>
  
  <?php } ?>

  <section class="bg-gray-light">
    <div class="container-lg resource-filters">

        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="resource-filter" class="form-group-lg">
            
            <div class="row mb-4">
                
                <div class="col-12 col-sm-4">
                   
                    <span class="meta text-primary d-inline-block mb-1 mb-md-2">Resource type</span><br />

                      <?php 

                        $terms = get_terms([
                            'taxonomy' => 'resource-type',
                            'hide_empty' => true
                        ]); 

                      ?>

                      <select name="type[]" class="selectpicker show-tick mb-4"  multiple data-max-options="1" data-style="btn-white" data-width="100%">  
                        <?php foreach($terms as $term) { ?>
                          <option value="<?php echo $term->term_id; ?>"  <?php if($_GET['type'] && in_array($term->term_id, $_GET['type'])){ ?>selected<?php } ?>><?php echo $term->name; ?></option>
                        <?php } ?>
                        </select>

                       
                </div>

                <div class="col-12 col-sm-4 pr-4">
                
                  <span class="meta text-primary d-inline-block mb-1 mb-md-2">Resource topic</span><br />
                  
                  <?php 

                    $terms = get_terms([
                      'taxonomy' => 'resource-topic',
                      'hide_empty' => true
                    ]); 

                  ?>

                  <select name="topics[]" class="selectpicker show-tick mb-4"  multiple data-max-options="1" data-style="btn-white" data-width="100%"> 
                    <?php foreach($terms as $term) { ?>
                      <option value="<?php echo $term->term_id; ?>" <?php if($_POST['topics'] && in_array($term->term_id, $_POST['topics'])){ ?>selected<?php } ?>><?php echo $term->name; ?></option>
                    <?php } ?>
                  </select>

                

                </div>

                <div class="col-12 col-sm-4 ml-auto">
                   
                <span class="meta text-primary d-inline-block mb-1 mb-md-2">Looking for something specific?</span><br />
                    <div style="position:relative">

                        <?php 
                          $new_term = stripslashes_deep($_POST['term']);
                        ?>
                        <input type="text" name="term" id="term" value="<?php echo $new_term; ?>" placeholder="Search term..." class="form-control bg-white border-0">

                        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-link">
                        
                          <span id="term_cancel" <?php if(!$_POST['term']){ ?>style="visibility: hidden"<?php } ?>><i class="fas fa-times-circle text-primary position-absolute" style="top: 15px; right: 15px;"></i></span>

                        </a>
                    </div>

                </div>
            </div>
            <input type="submit" class="btn btn-primary" value="Filter">
            <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="btn btn-link"><span class="meta text-primary">Reset</span></a>
        </form>

    </div>
</section>

<section>
  <div class="container-lg">

<?php if( $query->have_posts() ): ?>

        <div class="row mt-4">

            <?php while( $query->have_posts() ) : $query->the_post(); ?>

           @php
                $category_meta = get_categories_meta(get_post(), 'resource-topic');
                
            @endphp

            <div class="col-md-4 col-sm-6 col-12">

                    <a href="{{ the_permalink() }}">
                      @if (get_post_thumbnail_id())
                          {!! wp_get_attachment_image(get_post_thumbnail_id(), 'latest', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_sub_field('title')]) !!}
                      @else
                          <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3"
                              alt="{!! get_sub_field('title') !!}">
                      @endif
                  </a>

                    @if ($category_meta['parent'])
                      <span class="meta">{!! $category_meta['parent'] !!}</span>
                    @endif
                    <h4 class="mb-1"><a href="{{ the_permalink() }}">{!! the_title() !!}</a></h4>
                    <div class="meta mb-3">{{ get_reading_time(get_the_content()) }} min read</div>
                    <p>{{ the_excerpt() }}</p>

                </div> 
                
            <?php endwhile; ?>

            
          </div> 
          
          <?php endif; ?>
          
          <p><?php wp_pagenavi( array( 'query' => $query )); ?></p>
<?php // wp_reset_query();	 // Restore global post data  ?>

  </div>
</section>