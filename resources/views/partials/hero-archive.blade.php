@php
 $type = get_field('topic_intro_media', 'resource-topic_' . get_queried_object()->term_id); 
@endphp

<div class="hero bg-gradient-blue hero-short
    @if ( $type == 'film' )
    hero-film
    @endif
    ">
    <div>

        <img src="@asset('images/hero-mask-category.png')" class="hero-mask-category">

        <div class="container-lg">

            <?php if (get_field('hero_icon')) { ?>
            <img src="{{ the_field('hero_icon') }}" alt="<?php echo $hero_title; ?>"
                class="hero-icon" />
            <?php } ?>

            <div class="hero-content">

                <div class="row">

                    <div class="col-12 col-md-7 my-auto">
                      
                      <h1 class="mb-2">{{ single_cat_title() }}</h1>
                      
                      {!! category_description() !!}
                    </div>

                    <div class="col-12 col-md-5 my-auto
                        @if ( !get_field('topic_intro_image', 'resource-topic_' . get_queried_object()->term_id) && !get_field('topic_intro_film', 'resource-topic_' . get_queried_object()->term_id) )
                            d-none
                        @endif
                        ">

                        @switch( $type )

                            @case('image')
                                @if (get_field('topic_intro_image', 'resource-topic_' . get_queried_object()->term_id))
                                    {!! wp_get_attachment_image(get_field('topic_intro_image', 'resource-topic_' . get_queried_object()->term_id), 'card', false, ['class' => 'h-auto w-100 rounded', 'alt' => single_cat_title('', false)]) !!}
                                @endif
                            @break

                            @case('film')
                                @if (get_field('topic_intro_film', 'resource-topic_' . get_queried_object()->term_id))
                                    {!! do_shortcode('[arve url="' . get_field('topic_intro_film', 'resource-topic_' . get_queried_object()->term_id) . '" /]') !!}
                                @endif
                            @break

                            @default

                        @endswitch

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
