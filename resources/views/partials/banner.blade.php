<?php if ( get_field( 'banner-enabled', 'option' ) ): ?>

    <div class="top-banner d-flex justify-content-center align-items-center py-1" style="font-size: 0.9rem; background-color:<?php the_field('banner-colour', 'option') ?>">

        <div class="container-lg d-flex flex-column flex-md-row justify-content-between w-100">
            <?php if ( get_field( 'banner-text', 'option' ) ): ?>
                <div class="banner-text px-1 text-center">
                    <span class="mb-0" style="color:<?php the_field('banner-text_colour', 'option') ?>"><?php the_field('banner-text', 'option') ?></span>
                </div>
            <?php endif; ?>

            <?php if ( get_field( 'banner-button_text', 'option' ) ): ?>
                <div class="banner-btn px-1 text-center">
                    <a class="btn btn-sm px-2 py-1" href="<?php the_field('banner-button_link', 'option') ?>" style="background-color:<?php the_field('banner-button_colour', 'option') ?>; color: <?php the_field('banner-button_text_colour', 'option') ?>"><?php the_field('banner-button_text', 'option') ?></a>
                </div>
            <?php endif; ?>

            <?php if (function_exists('get_users_browsing_site')){ ?>
                @include('partials.live-visitors')
            <?php } ?>
        </div>

    </div>

<?php endif; // end of if field_name logic ?>