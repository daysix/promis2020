

<nav id="mobile-menu">
  <?php wp_nav_menu(
      array(
          'menu' => 'Main menu',
          'container' => false,
          'menu_id' => 'main-navigation',
          'menu_class' => false
      )
  ); ?>

<a href="/resources" class="btn btn-secondary text-white">View all resources +</a>
  
  <img src="@asset('images/corner.png')" id="mobile-nav-mask">
</nav>