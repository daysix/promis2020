
<?php

  $args = array(
    'post_type' => 'resource',
    'posts_per_page' => -1,
    'tax_query' =>  array(
      array (
        'taxonomy' => 'resource-type',
        'field' => 'term_id',
        'terms' => $resource_term_id
      )
    )
  );
      
      $query = new WP_Query($args);
      $posts = $query->posts;

      global $post;

      if($posts){ ?>

      <div class="post-listing">

      <div class="row">

      <?php foreach($posts as $post){
         
        setup_postdata($post); ?>

          <div class="col-12 col-md-6 col-lg-4 mb-4 mb-md-5">
            <a href="<?php the_permalink(); ?>" class="no-hover">
            <div class="card cursor h-100">

              <?php // echo 'IMAGE: ' . $post->ID . ' - ' . get_sub_field('resource-image', $post->ID); ?>

              <?php echo wp_get_attachment_image( get_field('resource-image', $post->ID), 'full', false, array('class' => 'w-100 h-auto rounded-top', 'alt' => get_sub_field('title', $post->ID)) ); ?>

              <div class="card-body bg-gray-light rounded <?php if(get_sub_field('resource-image', $post->ID)){ ?>-bottom<?php } ?>">

                <?php // echo wp_get_attachment_image( get_sub_field('icon'), 'profile', false, array('class' => 'mb-4', 'alt' => get_sub_field('title')) ); ?>

                <h4 class="mb-2 text-primary"><?php the_title(); ?></h4>

                <p><?php echo get_the_excerpt($post->ID); ?></p>

              </div>

            </div>

          </a>

          </div>

            <?php }
            wp_reset_postdata(); ?>

            </div>

        </div>

      <?php } ?>
