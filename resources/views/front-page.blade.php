@extends('layouts.app')

@section('content')
    @while (have_posts()) @php the_post() @endphp

        <section>
            <div class="container-lg">
                <h2 class="mb-4 mb-md-5">Featured resources</h2>
                <div class="row">
                    <div class="col-12 col-md-8">

                        @if (have_rows('featured_resources'))

                            @php $stacked = true; @endphp

                            <div class="row">
                                @while (have_rows('featured_resources')) @php the_row() @endphp

                                    {{-- @if (get_row_index() == 1)
                                    <div class="col-6">
                                @endif --}}

                                    <div class="col-md-6 ">

                                        @php
                                            $resource = get_sub_field('resource');
                                            $category_meta = get_categories_meta($resource, 'resource-topic');
                                        @endphp

                                        @include('parts.resource')

                                    </div>

                                    {{-- @if (get_row_index() == 1)
                                    </div>
                                        <div class="col-6">
                                @endif --}}

                                @endwhile
                                {{-- </div> --}}
                            </div>

                            @php $stacked = false; @endphp

                        @endif

                    </div>
                    <div class="col-12 col-md-4">
                        <h3>What are you looking for?</h3>
                        <p>We've curated some of the most popular resources in the areas below</p>

                        @php
                            $terms = get_terms('audiences', ['hide_empty' => false]);
                        @endphp
                        @foreach ($terms as $term)

                            <div class="icon">
                                <img src="{{ the_field('audience-icon', 'term_' . $term->term_id) }}">
                                <div>
                                    <h4><a href="{{ get_term_link($term->term_id) }}">{{ $term->description }}</a></h4>
                                </div>
                            </div>

                        @endforeach

                        @if (have_rows('related_areas'))

                            <h3>Areas of interest</h3>
                            <div class="tags mt-3 mb-4">

                                @while (have_rows('related_areas'))
                                    @php (the_row()) @endphp

                                    @php
                                        if (get_sub_field('type') == 'link') {
                                            $title = get_sub_field('title');
                                            $link = get_sub_field('link');
                                        } else {
                                            $topic = get_sub_field('topic');
                                            $title = $topic->name;
                                            $link = get_term_link($topic);
                                        }
                                    @endphp

                                    <a href="{{ $link }}"
                                        class="btn btn-secondary related-area">{{ $title }}</a>

                                @endwhile

                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </section>

        <section class="bg-gradient-blue">
            <div class="container-lg">
                <div class="row">
                    <div class="col-12 col-md-5">
                        <h3>Introducing the Wellbeing Hub</h3>
                        <p>We spend our days caring for others but sometimes we're not good at asking for help ourselves.</p>
                            <p>That's why we’ve created the National Wellbeing Hub. It’s a place full of ideas on how to stay well. </p>
                            <p>Take a look around or listen to a story and find something that will help you.</p>
                    </div>
                    <div class="col-12 col-md-7">
                        <div>
                            {!! do_shortcode('[arve url="https://vimeo.com/635216889" /]') !!}


                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container-lg">

                <div class="row">
                    <div class="col-12 col-md-8">
                        <h2 class="mb-4 mb-md-5">Latest on the hub</h2>

                        @php
                            
                            $args = [
                                'post_type' => 'resource',
                                'posts_per_page' => 5,
                                'orderby' => 'publish_date',
                                'order' => 'desc',
                            ];
                            
                            $latest_query = new WP_Query($args);
                            
                        @endphp

                        @if ($latest_query->have_posts())

                            <div class="resource-container">

                                <div class="mt-4">

                                    @while ($latest_query->have_posts())
                                        @php $latest_query->the_post() @endphp

                                        @php
                                            $resource = get_post();
                                            $category_meta = get_categories_meta(get_post(), 'resource-topic');
                                        @endphp

                                        @include('parts.resource')

                                    @endwhile

                                </div>

                            </div>

                            @php wp_reset_postdata() @endphp

                        @endif

                    </div>
                    <div class="col-12 col-md-4">
                        <h3 class="mb-4 mb-md-5">Most popular</h3>

                        @php
                            
                            $popular_resources = pvc_get_most_viewed_posts([
                                'order' => 'desc',
                                'post_type' => ['resource'],
                                'posts_per_page' => 8,
                            ]);
                            
                        @endphp

                        @if ($popular_resources)
                            @foreach ($popular_resources as $post)

                                @php
                                    $category_meta = get_categories_meta($post, 'resource-topic');
                                @endphp

                                <div class="row no-gutters mb-4">
                                    <div class="col-4">


                                        <a href="{{ get_the_permalink($post->ID) }}">
                                            @if (get_post_thumbnail_id($post->ID))
                                                {!! wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'square', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_the_title($post->ID)]) !!}
                                            @else
                                                <img src="@asset('images/placeholder/square-placeholder-' . rand(1,4) . '.png')"
                                                    class="w-100 h-auto rounded mb-3" alt="{!! get_sub_field('title') !!}">
                                            @endif
                                        </a>

                                    </div>
                                    <div class="col-8">
                                        <div class="ml-4">
                                            @if ($category_meta['parent'])
                                                <span class="meta">{!! $category_meta['parent'] !!}</span>
                                            @endif
                                            <h5 class="mb-1"><a
                                                    href="{{ get_the_permalink($post->ID) }}">{!! get_the_title($post->ID) !!}</a>
                                            </h5>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                            @php wp_reset_postdata() @endphp

                        @endif

                    </div>
                </div>
            </div>
        </section>

        {{-- @include('partials.content-page') --}}

    @endwhile
@endsection
