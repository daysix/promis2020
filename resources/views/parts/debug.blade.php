<?php
    function getWPVersion() {
        //$path = get_home_path();
        include ABSPATH.'/wp-includes/version.php';
        return $wp_version;
    }
?>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<script>
   var letter = {
        d: 68,
        z: 90
    };
    jQuery(document).ready(function() {
        jQuery("body").keydown(function(event){
            if(event.ctrlKey && event.keyCode == letter.d){
                jQuery("#debug-container").slideToggle("fast");
            }
        });
    });
</script>
<style>
    #debug-container{
        display:none;
        font-family: 'Lato', sans-serif;
        background-color: white;
        color:black;
        position:fixed;
        width: 100%;
        z-index: 99999999999;
        padding:0 20px 10px;
        border-bottom:1px solid #ccc;
        font-size: 13px;
    }
    #debug-container a{
        color: #E22430;
    }
    #debug-container p{
        padding:5px 0 0;
    }
    #debug-container strong{
        color: #E22430;
    }
    #debug-container small{
        color: #ccc;
    }
</style>
<div id="debug-container">
    <p><strong></strong></p>
    <p><strong>Template File:</strong> <?php global $template; echo $template; ?> | <a href="https://developer.wordpress.org/files/2014/10/template-hierarchy.png" target="_blank">Template Hierarchy</a></p>
    <p>
        <strong>WP version:</strong> <?php echo getWPVersion(); ?><br />
        <strong>PHP version</strong> <?php echo phpversion(); ?>
    </p>
</div>