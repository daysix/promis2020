@php $mm = get_field('mega_menu', $menu_item) @endphp

{!! apply_filters('the_content', $mm->post_content) !!}

