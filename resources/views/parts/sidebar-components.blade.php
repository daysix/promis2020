@if( have_rows('sidebar_components') )
@while( have_rows('sidebar_components') )
  @php (the_row())

  <?php 

  switch(get_row_layout()) {
    case "card":
      $classes = "mb-4 mb-md-5";
      break;
    case "testimonials":
      $classes = "mb-3 mb-md-4";
      break;
  }

  ?>


  <div class="col-12 mb-5 {{ $classes }}">
    <div class="component-sidebar-{{ get_row_layout() }} _h-100">
      @include('components.sidebar-'.get_row_layout())
    </div>
  </div>

@endwhile
@endif