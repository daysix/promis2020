@php
    if( !isset($stacked) ) {
        $stacked = false;
    }
@endphp

<div class="row mb-5">
  <div class="col-12 
    @if( !$stacked )
     col-md-6
    @endif
    ">

      <div class="position-relative">

          {!! get_the_resource_icon($resource) !!}

          <a href="{{ get_the_permalink($resource->ID) }}" class="position-relative">
              @if (get_field('featured_film', $resource->ID))
                  {!! do_shortcode('[arve url="' . get_field('featured_film', $resource->ID) . '" /]') !!}
              @elseif (get_post_thumbnail_id($resource->ID))
                  {!! wp_get_attachment_image(get_post_thumbnail_id($resource->ID), 'full', false, ['class' => 'w-100 h-auto rounded mb-3', 'alt' => get_the_title($resource->ID)]) !!}
              @else
                  <img src="@asset('images/placeholder/16x9-placeholder-' . rand(1,5) . '.png')" class="w-100 h-auto rounded mb-3"
                      alt="{!! get_the_title($resource->ID) !!}">
              @endif
          </a>

      </div>

  </div>

  
  <div class="col-12
  @if( !$stacked )
    col-md-6
    @endif
    ">
      @if ($category_meta['parent'])
          <span class="meta">{!! $category_meta['parent'] !!}</span>
      @endif
      <h4 class="mb-1"><a
              href="{{ get_the_permalink($resource->ID) }}">{!! get_the_title($resource->ID) !!}</a>
      </h4>
      <div class="meta mb-3">{{ get_reading_time($resource->post_content) }} min read
      </div>
      @if( !$stacked )
      <p>
          {!! get_the_excerpt($resource) !!}
      </p>
      @endif
      
  </div>
</div>