@if (have_rows('sections'))

  @while (have_rows('sections'))
    @php (the_row())

    <?php 
    
    $base = 12/get_sub_field('columns');

    if(get_sub_field('gutters') == 'no') {
      $gutters = 'no-gutters';
    } else {
      $gutters = '';
    }
    
    if(get_sub_field('width')){
      $width = get_sub_field('width');
    }else{
      $width = get_field('page_width');
    }

    ?>

    <section 
    
      @if (get_sub_field('background') === 'image')
        style="background-image:url({{ the_sub_field('background_image') }};)"
      @endif

      class="

      {{ $gutters }}

      bg-{{ the_sub_field('background') }}

      text-{{ the_sub_field('alignment') }}

      @if (get_sub_field('background') === 'image' && get_sub_field('background_image'))
        bg-image
      @endif

      @if (get_sub_field('no_padding'))
        no_padding
      @endif

      @if (get_sub_field('background') === 'image' && get_sub_field('background_image') && get_sub_field('overlay') == 'yes')
        overlay
      @endif

      ">
      <div>
        <div class="{{ $width }}">
          <div class="row {{ $gutters }}">

              @if( have_rows('components') )
                @while( have_rows('components') )
                  @php (the_row())

                  <?php 

                  switch(get_row_layout()) {
                    case "card":
                      $classes = "mb-4 mb-md-5";
                      break;
                    case "testimonials":
                      $classes = "mb-3 mb-md-4";
                      break;
                  }

                  ?>

                  @if (get_sub_field('columns_span'))

                    <?php
                    if( get_sub_field('columns_span') !== 'max' ) {
                      $col = $base * get_sub_field('columns_span');
                    } else {
                      $col = '12';
                    }
                    ?>

                  @else
                    <?php $col = $base; ?>
                  @endif

                  <div 
                    @if (get_sub_field('component_background_image'))
                      style="background-image:url({{ the_sub_field('component_background_image') }};)"
                    @endif
                    class="col-12 col-md-{{ $col }} {{ $classes }} {{ get_sub_field('css_classes') }}
                    @if (get_sub_field('component_background_image'))
                      bg-image
                    @endif
                    ">
                    <div class="component-{{ get_row_layout() }} _h-100">
                      @include('components.'.get_row_layout())
                    </div>
                  </div>

                @endwhile
              @endif
            
          </div>
        </div>
      </div>
    </section>

  @endwhile

@endif