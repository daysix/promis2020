@if (have_rows('buttons'))

  <div class="buttons mt-4">

  @while (have_rows('buttons'))
    @php (the_row())

    <?php
    $link = "";
    if(get_sub_field('type') == 'page') {
      $link = get_the_permalink(get_sub_field('page')->ID);
    } else {
      $link = get_sub_field('link');
    }
    ?>

    <a href="<?php echo $link; ?>" class="btn btn-{{ the_sub_field('color') }}">
      {{ the_sub_field('text') }}
    </a>

  @endwhile

  </div>

@endif