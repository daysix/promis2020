<?php $type = get_sub_field('type'); ?>

@if (have_rows('items'))

    <?php if($type == 'v-pills'){ ?>

    <div class="row">
      <div class="col-3">

      <?php } else { ?>

        <nav>

      <?php } ?>

    <?php $n = 1; ?>

        <div class="nav <?php if($type == 'v-pills'){ ?>flex-column<?php } ?> nav-<?php if($type == 'tabs'){ ?>tabs<?php }else { ?>pills<?php } ?>" id="nav-tab" role="tablist" <?php if($type == 'v-pills'){ ?>aria-orientation="vertical"<?php } ?>>

    @while (have_rows('items'))
      @php (the_row())
      
        <a class="nav-item nav-link <?php if($n == 1){ ?>active<?php } ?>" id="nav-tab-{{ $n }}" data-toggle="tab" href="#nav-{{ $n }}" role="tab" aria-controls="nav-{{ $n }}" aria-selected="true">{{ the_sub_field('title') }}</a>

        <?php ob_start(); ?>

        <div class="tab-pane fade <?php if($n == 1){ ?>show active<?php } ?>" id="nav-{{ $n }}" role="tabpanel" aria-labelledby="nav-tab-{{ $n }}">

          {{ the_sub_field('content') }}

          @include('parts.buttons')

        </div>

        <?php $content .= ob_get_clean(); ?>

      <?php $n++; ?>

    @endwhile

    </div>

    <?php if($type == 'v-pills'){ ?>

      </div>
    <div class="col-9">
  
    <?php } else { ?>
  
      </nav>
  
    <?php } ?>
  

    <div class="tab-content <?php if($type !== 'v-pills'){ ?>pt-4<?php } ?>" id="nav-tabContent">

      <?php echo $content ?>

    </div>

    <?php if($type == 'v-pills'){ ?>

    </div>

    <?php } ?>

@endif
